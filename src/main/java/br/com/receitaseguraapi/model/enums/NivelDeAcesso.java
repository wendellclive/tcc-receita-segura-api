package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum NivelDeAcesso {

	ADMINISTRADOR(10, "ADMINISTRADOR"),
	CONSELHO(20, "CONSELHO DE CLASSE"),
	GOVERNO(30, "GOVERNO"),
	PROFISSIONAL(40, "MEDICO");
	
	private int codigo;
	private String descricao;
	
	public static NivelDeAcesso toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (NivelDeAcesso x : NivelDeAcesso.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
			
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
