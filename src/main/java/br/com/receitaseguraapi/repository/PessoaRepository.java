package br.com.receitaseguraapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.receitaseguraapi.model.general.Medico;
import br.com.receitaseguraapi.model.general.Paciente;
import br.com.receitaseguraapi.model.general.Pessoa;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

	@Query("select rs from Pessoa rs where id= :id and tipoPessoa=1")
	public Optional<Medico> buscarPorMedicoPorId(Integer id);

	@Query("select rs from Pessoa rs where tipoPessoa=1")
	public List<Medico> buscarTodosMedicos();

	@Query("select rs from Pessoa rs where tipoPessoa=1")
	public Page<Medico> buscarTodosMedicosPaginado(Pageable pageable);

	@Query("select rs from Pessoa rs where id= :id and tipoPessoa=2")
	public Optional<Paciente> buscarPorPacientePorId(Integer id);

	@Query("select rs from Pessoa rs where tipoPessoa=2")
	public List<Paciente> buscarTodosPacientes();

	@Query("select rs from Pessoa rs where tipoPessoa=2")
	public Page<Paciente> buscarTodosPacientesPaginado(Pageable pageable);

}
