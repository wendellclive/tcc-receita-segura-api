package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.receitaseguraapi.model.enums.TipoPessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa extends Sequencial implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;
	private String cpf;
	private String rg;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pessoa_id", referencedColumnName = "id")
	private Endereco endereco;
	
	private Integer tipoPessoa;
	
	public Pessoa(String nome, String cpf, String rg, TipoPessoa tipoPessoa) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.tipoPessoa = tipoPessoa.getCodigo();
	}

	@NonNull
	public TipoPessoa getTipoPessoa() {

		return TipoPessoa.toEnum(tipoPessoa);

	}

	@NonNull
	public void setTipoPessoa(TipoPessoa tipoPessoa) {

		this.tipoPessoa = tipoPessoa.getCodigo();

	}
	
}
