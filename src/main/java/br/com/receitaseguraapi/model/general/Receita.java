package br.com.receitaseguraapi.model.general;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import br.com.receitaseguraapi.model.enums.StatusDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue(value = "receita_simples")
public class Receita extends Documento implements Serializable {

	private static final long serialVersionUID = 1L;

	private LocalDateTime dataValidade;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "prescricao_id")
	private List<Prescricao> prescricoes;

	public Receita(Medico medico, Paciente paciente, String assinatura, String chaveAutenticacao, StatusDoc statusDoc,
			String autorizacao, LocalDateTime dataValidade, List<Prescricao> prescricoes) {
		super(medico, paciente, assinatura, chaveAutenticacao, statusDoc, autorizacao);
		this.dataValidade = dataValidade;
		this.prescricoes = prescricoes;
	}
	
	
	
}
