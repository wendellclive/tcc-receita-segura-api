package br.com.receitaseguraapi.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Conversor {

	public static String converteDateToString() {

		Date dataAtual = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/ddss:mm:HH");
		String dataFormatada = dateFormat.format(dataAtual);

		System.out.println("Date em String formatada: " + dataFormatada);

		String dataLimpa = dataFormatada.replace("/", "");

		return dataLimpa.replace(":", "");

	}

	public static Date converterParaData(LocalDateTime localDate) {

		Date date = Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant());

		return date;
	}	

}
