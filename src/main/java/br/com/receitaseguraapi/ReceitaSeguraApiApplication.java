package br.com.receitaseguraapi;

import java.util.TimeZone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.receitaseguraapi.config.property.ReceitaSeguraApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(ReceitaSeguraApiProperty.class)
public class ReceitaSeguraApiApplication 
//extends SpringBootServletInitializer 
implements CommandLineRunner {
	
	public static void main(String[] args) {
		SpringApplication.run(ReceitaSeguraApiApplication.class, args);
		
		TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
		
	}
/*
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ReceitaSeguraApiApplication.class);
	}
*/
	@Override
	public void run(String... args) throws Exception {
	
		
	}
	
}
