package br.com.receitaseguraapi.services;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.github.javafaker.Faker;

import br.com.receitaseguraapi.model.enums.NivelDeAcesso;
import br.com.receitaseguraapi.model.enums.StatusDoc;
import br.com.receitaseguraapi.model.enums.StatusUsuario;
import br.com.receitaseguraapi.model.enums.TipoInstituicao;
import br.com.receitaseguraapi.model.enums.TipoPessoa;
import br.com.receitaseguraapi.model.enums.TipoUsuario;
import br.com.receitaseguraapi.model.general.Atestado;
import br.com.receitaseguraapi.model.general.Cid;
import br.com.receitaseguraapi.model.general.Documento;
import br.com.receitaseguraapi.model.general.Endereco;
import br.com.receitaseguraapi.model.general.Especialidade;
import br.com.receitaseguraapi.model.general.Instituicao;
import br.com.receitaseguraapi.model.general.Medicamento;
import br.com.receitaseguraapi.model.general.Medico;
import br.com.receitaseguraapi.model.general.Paciente;
import br.com.receitaseguraapi.model.general.Prescricao;
import br.com.receitaseguraapi.model.general.Receita;
import br.com.receitaseguraapi.model.usuario.Usuario;
import br.com.receitaseguraapi.repository.CidRepository;
import br.com.receitaseguraapi.repository.DocumentoRepository;
import br.com.receitaseguraapi.repository.EnderecoRepository;
import br.com.receitaseguraapi.repository.EspecialidadeRepository;
import br.com.receitaseguraapi.repository.InstituicaoRepository;
import br.com.receitaseguraapi.repository.MedicamentoRepository;
import br.com.receitaseguraapi.repository.PessoaRepository;
import br.com.receitaseguraapi.repository.UsuarioRepository;
import br.com.receitaseguraapi.utils.GeraAutorizacao;
import br.com.receitaseguraapi.utils.Permissoes;

@Service
public class DBService {

	@Autowired
	private EnderecoRepository enderecoRepository;

	@Autowired
	private CidRepository cidRepository;

	@Autowired
	private MedicamentoRepository medicamentoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private InstituicaoRepository instituicaoRepository;

	@Autowired
	private EspecialidadeRepository especialidadeRepository;

	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private DocumentoRepository documentoRepository;

	@Autowired
	private PasswordEncoder pe;

	public void instantiateTestDatabase() throws ParseException {

		Faker faker = new Faker(new Locale("pt-BR"));

		Instituicao inst0 = new Instituicao(faker.number().digits(14), "TCC UNIPE ALUNOS",
				TipoInstituicao.GESTOR_SISTEMA);
		Instituicao inst1 = new Instituicao(faker.number().digits(14), "CRM-PB", TipoInstituicao.CONSELHO);
		Instituicao inst2 = new Instituicao(faker.number().digits(14), "CRM-PE", TipoInstituicao.CONSELHO);
		Instituicao inst3 = new Instituicao(faker.number().digits(14), "CRM-RN", TipoInstituicao.CONSELHO);
		Instituicao inst4 = new Instituicao(faker.number().digits(14), "CRM-AL", TipoInstituicao.CONSELHO);

		Endereco end0 = new Endereco(faker.address().streetName(), faker.address().cityName(),
				faker.address().zipCode(), faker.address().city(), faker.address().state(), "1200", null, inst0, null);
		Endereco end1 = new Endereco(faker.address().streetName(), faker.address().cityName(),
				faker.address().zipCode(), faker.address().city(), faker.address().state(), "1200", null, inst1, null);
		Endereco end2 = new Endereco(faker.address().streetName(), faker.address().cityName(),
				faker.address().zipCode(), faker.address().city(), faker.address().state(), "1200", null, inst2, null);
		Endereco end3 = new Endereco(faker.address().streetName(), faker.address().cityName(),
				faker.address().zipCode(), faker.address().city(), faker.address().state(), "1200", null, inst3, null);
		Endereco end4 = new Endereco(faker.address().streetName(), faker.address().cityName(),
				faker.address().zipCode(), faker.address().city(), faker.address().state(), "1200", null, inst4, null);

		inst0.setEndereco(end0);
		inst1.setEndereco(end1);
		inst2.setEndereco(end2);
		inst3.setEndereco(end3);
		inst4.setEndereco(end4);

		instituicaoRepository.saveAll(Arrays.asList(inst0, inst1, inst2, inst3, inst4));
		enderecoRepository.saveAll(Arrays.asList(end0, end1, end2, end3, end4));

		Usuario usu1 = new Usuario("wendell", pe.encode("1"), "fulano@gmail.com", "WENDELL CLIVE", StatusUsuario.ATIVO,
				0, true, NivelDeAcesso.ADMINISTRADOR, TipoUsuario.ADMINISTRADOR, inst0);
		Usuario usu2 = new Usuario("israel", pe.encode("1"), "fulano@gmail.com", "ISRAEL ARAUJO", StatusUsuario.ATIVO,
				0, true, NivelDeAcesso.ADMINISTRADOR, TipoUsuario.ADMINISTRADOR, inst0);

		Permissoes permissoes = new Permissoes();

		usu1 = permissoes.adicionaPermissao(usu1);
		usu2 = permissoes.adicionaPermissao(usu2);

		usuarioRepository.saveAll(Arrays.asList(usu1, usu2));

		Cid cid1 = new Cid("A00", "Colera");
		Cid cid2 = new Cid("A01", "Febres tifoide e paratifoide");
		Cid cid3 = new Cid("A02", "Outr infecc p/Salmonella");
		Cid cid4 = new Cid("A03", "Shiguelose");
		Cid cid5 = new Cid("A04", "Outr infecc intestinais bacter");
		Cid cid6 = new Cid("A05", "Outr intox alimentares bacter NCOP");
		Cid cid7 = new Cid("A06", "Amebiase");
		Cid cid8 = new Cid("A07", "Outr doenc intestinais p/protozoarios");
		Cid cid9 = new Cid("A08", "Infecc intestinais virais outr e as NE");
		Cid cid10 = new Cid("A09", "Diarreia e gastroenterite orig infecc presum");

		cidRepository.saveAll(Arrays.asList(cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8, cid9, cid10));

		Medicamento med1 = new Medicamento("AZITROMICINA", "56998982000107", "BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA",
				"AZITROMICINA", "250 MG PO LIOF INJ CT FA + SER DESC", "M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS",
				"Biológicos", true, "Tarja Vermelha");
		Medicamento med2 = new Medicamento("BACTRIM", "56998982000107", "BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA",
				"BACTRIM", "125 MG/ML SOL INJ CT 1 SER PREENCHIDA", "M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS",
				"Biológicos", false, "Tarja Vermelha");
		Medicamento med3 = new Medicamento("ALMOXICILINA", "56998982000107", "BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA",
				"ALMOXICILINA", "125 MG/ML SOL INJ CT 4 SER PREENCHIDA", "M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS",
				"Biológicos", false, "Tarja Vermelha");
		Medicamento med4 = new Medicamento("CIPROFLOXACINO", "56998982000107", "BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA",
				"CIPROFLOXACINO", "125 MG/ML SOL INJ CT 1 SER PREENCHIDA + DISPOSITIVO ULTRASAFE",
				"M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS", "Biológicos", false, "Tarja Vermelha");
		Medicamento med5 = new Medicamento("DIPIRONA SÓDICA", "56998982000107",
				"BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA", "DIPIRONA SÓDICA",
				"125 MG/ML SOL INJ CT 4 SER PREENCHIDA + DISPOSITIVO ULTRASAFE",
				"M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS", "Biológicos", false, "Tarja Vermelha");
		Medicamento med6 = new Medicamento("PARACETAMOL", "56998982000107", "BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA",
				"PARACETAMOL",
				"125 MG/ML SOL INJ CT 1 SER PREENC VD TRANS + DISPOSITIVO ULTRASAFE PASSIVE + EXTENSORES DE APOIO",
				"M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS", "Biológicos", false, "Tarja Vermelha");
		Medicamento med7 = new Medicamento("CETOPROFENO", "56998982000107", "BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA",
				"CETOPROFENO",
				"125 MG/ML SOL INJ CT 4 SER PREENC VD TRANS + DISPOSITIVO ULTRASAFE PASSIVE + EXTENSORES DE APOIO",
				"M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS", "Biológicos", false, "Tarja Vermelha");
		Medicamento med8 = new Medicamento("DIMETICONA", "43940618000144", "ELI LILLY DO BRASIL LTDA", "DIMETICONA",
				"2 MG/ML SOL INJ CT FA VD INC X 5 ML",
				"B01C3 - INIBIDORES DA AGREGAÇÃO PLAQUETÁRIA, ANTAGONISTAS GLICOPROTEÍNA IIB/IIIA", "Biológicos", true,
				"Tarja Vermelha");
		Medicamento med9 = new Medicamento("CECLOR", "51780468000187", "JANSSEN-CILAG FARMACÊUTICA LTDA", "CECLOR",
				"2 MG/ML SOL INJ CT FA VD INC X 5 ML",
				"B01C3 - INIBIDORES DA AGREGAÇÃO PLAQUETÁRIA, ANTAGONISTAS GLICOPROTEÍNA IIB/IIIA", "Biológico Novo",
				true, "Tarja Vermelha");
		Medicamento med10 = new Medicamento("AAS", "00923140000131", "EMS SIGMA PHARMA LTDA", "AAS",
				"100 MG COM CT 3 BL AL PVDC LEIT X 10", "A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE",
				"Biológicos", false, "Tarja Vermelha");
		Medicamento med11 = new Medicamento("ACICLOVIR", "18459628000115", "BAYER S.A.", "ACICLOVIR",
				"100 MG COM CT BL AL/AL X 30", "A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE", "Biológicos",
				false, "Tarja Vermelha");
		Medicamento med12 = new Medicamento("SULFATO DE SALBUTAMOL", "00923140000131", "EMS SIGMA PHARMA LTDA",
				"SULFATO DE SALBUTAMOL", "50 MG COM CT 3 BL AL PVDC LEIT X 10", "A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE",
				"Biológicos", false, "Tarja Vermelha");
		Medicamento med13 = new Medicamento("LORATADINA", "18459628000115", "BAYER S.A.", "LORATADINA",
				"50 MG COM CT BL AL/AL X 30", "A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE", "Biológicos", false,
				"Tarja Vermelha");
		Medicamento med14 = new Medicamento("LEVOFLOXACINO", "61190096000192", "EUROFARMA LABORATÓRIOS S.A.",
				"LEVOFLOXACINO", "5 MG/ML GEL OR CT FR PLAS INC X 120ML + COL MED", "R05C0 - EXPECTORANTES",
				"Novo (Referência)", false, "Tarja Vermelha");
		Medicamento med15 = new Medicamento("IBUPROFENO", "02814497000107", "CIMED INDÚSTRIA DE MEDICAMENTOS LTDA",
				"IBUPROFENO", "10 MG/ML XPE CT FR PLAS AMB X 120 ML + CP MED", "R05C0 - EXPECTORANTES", "Genérico",
				false, "Tarja Vermelha");
		Medicamento med16 = new Medicamento("HIDROXICLOROQUINA", "02814497000107",
				"CIMED INDÚSTRIA DE MEDICAMENTOS LTDA", "HIDROXICLOROQUINA",
				"10 MG/ML XPE CX 50 FR PLAS AMB X 120 ML + 50 CP MED (EMB HOSP)", "R05C0 - EXPECTORANTES", "Genérico",
				true, "Tarja Vermelha");

		medicamentoRepository.saveAll(Arrays.asList(med1, med2, med3, med4, med5, med6, med7, med8, med9, med10, med11,
				med12, med13, med14, med15, med16));

		Especialidade esp1 = new Especialidade("Acupuntura");
		Especialidade esp2 = new Especialidade("Alergia");
		Especialidade esp3 = new Especialidade("Anestesiologia");
		Especialidade esp4 = new Especialidade("Angiologia");
		Especialidade esp5 = new Especialidade("Oncologia)");
		Especialidade esp6 = new Especialidade("Cardiologia");
		Especialidade esp7 = new Especialidade("Cirurgia Cardiovascular");
		Especialidade esp8 = new Especialidade("Cirurgia da Mão");
		Especialidade esp9 = new Especialidade("Cirurgia de Cabeça e Pescoço");
		Especialidade esp10 = new Especialidade("Cirurgia do Aparelho Digestivo");
		Especialidade esp11 = new Especialidade("Cirurgia Geral");
		Especialidade esp12 = new Especialidade("Cirurgia Pediátrica");
		Especialidade esp13 = new Especialidade("Cirurgia Plástica");
		Especialidade esp14 = new Especialidade("Cirurgia Torácica");
		Especialidade esp15 = new Especialidade("Cirurgia Vascular");
		Especialidade esp16 = new Especialidade("Clínica Médica ");
		Especialidade esp17 = new Especialidade("Coloproctologia");
		Especialidade esp18 = new Especialidade("Dermatologia");
		Especialidade esp19 = new Especialidade("Endocrinologia");
		Especialidade esp20 = new Especialidade("Endoscopia");
		Especialidade esp21 = new Especialidade("Gastroenterologia");
		Especialidade esp22 = new Especialidade("Genética Médica");
		Especialidade esp23 = new Especialidade("Geriatria");
		Especialidade esp24 = new Especialidade("Ginecologia e obstetrícia");
		Especialidade esp25 = new Especialidade("Hematologia e Hemoterapia");
		Especialidade esp26 = new Especialidade("Homeopatia");
		Especialidade esp27 = new Especialidade("Infectologia");
		Especialidade esp28 = new Especialidade("Mastologia");
		Especialidade esp30 = new Especialidade("Medicina de Emergência");
		Especialidade esp31 = new Especialidade("Medicina do Trabalho");
		Especialidade esp32 = new Especialidade("Medicina do Tráfego");
		Especialidade esp33 = new Especialidade("Medicina Esportiva");
		Especialidade esp35 = new Especialidade("Medicina Intensiva");
		Especialidade esp37 = new Especialidade("Medicina Nuclear");
		Especialidade esp38 = new Especialidade("Medicina Preventiva");
		Especialidade esp39 = new Especialidade("Nefrologia");
		Especialidade esp40 = new Especialidade("Neurocirurgia");
		Especialidade esp41 = new Especialidade("Neurologia");
		Especialidade esp42 = new Especialidade("Nutrologia");
		Especialidade esp43 = new Especialidade("Obstetrícia");
		Especialidade esp44 = new Especialidade("Oftalmologia");
		Especialidade esp49 = new Especialidade("Pediatria");
		Especialidade esp50 = new Especialidade("Pneumologia");
		Especialidade esp51 = new Especialidade("Psiquiatria");
		Especialidade esp53 = new Especialidade("Radioterapia");
		Especialidade esp54 = new Especialidade("Reumatologia");
		Especialidade esp55 = new Especialidade("Urologia");

		especialidadeRepository.saveAll(Arrays.asList(esp1, esp2, esp3, esp4, esp5, esp6, esp7, esp8, esp9, esp10,
				esp11, esp12, esp13, esp14, esp15, esp16, esp17, esp18, esp19, esp20, esp21, esp22, esp23, esp24, esp25,
				esp26, esp27, esp28, esp30, esp31, esp32, esp33, esp35, esp37, esp38, esp39, esp40, esp41, esp42, esp43,
				esp44, esp49, esp50, esp51, esp53, esp54, esp55));

		Medico med = new Medico();
		Paciente pac = new Paciente();
		Endereco ende = new Endereco();
		Endereco ender = new Endereco();
		Documento atestado = new Atestado();
		Documento receita = new Receita();
		Prescricao p1, p2, p3 = new Prescricao();
		Usuario usu = new Usuario();
		Permissoes permissoes2 = new Permissoes();
		
		for (int i = 1; i < 31; i++) {

			med = new Medico(faker.name().fullName(), faker.number().digits(11), faker.number().digits(7),
					TipoPessoa.MEDICO, faker.number().digits(4), esp1, inst0);

			ende = new Endereco(faker.address().streetName(), faker.address().cityName(), faker.address().zipCode(),
					faker.address().city(), faker.address().state(), faker.address().buildingNumber(), null, null, med);

			pac = new Paciente(faker.name().fullName(), faker.number().digits(11), faker.number().digits(7),
					TipoPessoa.PACIENTE);

			ender = new Endereco(faker.address().streetName(), faker.address().cityName(), faker.address().zipCode(),
					faker.address().city(), faker.address().state(), faker.address().buildingNumber(), null, null, pac);

			med.setEndereco(ende);
			pac.setEndereco(ender);

			atestado = new Atestado(med, pac, faker.crypto().md5(), faker.crypto().md5(), StatusDoc.VALIDO,
					GeraAutorizacao.nomeAleatorio(5), true, cid1, 15);

			p1 = new Prescricao(med16, "Tomar 1caixa ao dia 12/12h");
			p2 = new Prescricao(med1, "Tomar 20ml ao dia 6/6h");
			p3 = new Prescricao(med8, "Tomar 5ml ao dia 12/12h");

			receita = new Receita(med, pac, faker.crypto().md5(), faker.crypto().md5(), StatusDoc.VALIDO,
					GeraAutorizacao.nomeAleatorio(5), LocalDateTime.now().plusDays(60), Arrays.asList(p1, p2, p3));
			String x = String.valueOf(i++);
			usu = new Usuario(x, pe.encode("1"), "fulano@gmail.com", med.getNome(), StatusUsuario.ATIVO,
					0, true, NivelDeAcesso.ADMINISTRADOR, TipoUsuario.ADMINISTRADOR, inst0);

			usu = permissoes2.adicionaPermissao(usu);
			
			pessoaRepository.save(med);
			enderecoRepository.save(ende);
			pessoaRepository.save(pac);
			enderecoRepository.save(ender);
			documentoRepository.save(atestado);
			documentoRepository.save(receita);
			usuarioRepository.save(usu);
			
		}

	}

}
