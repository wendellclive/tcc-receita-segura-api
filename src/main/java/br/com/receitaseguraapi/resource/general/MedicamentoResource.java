package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.MEDICAMENTOS;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.receitaseguraapi.model.general.Cid;
import br.com.receitaseguraapi.services.CidService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@Api(value = MEDICAMENTOS, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_MEDICAMENTO })
@RequestMapping(value = MEDICAMENTOS, produces = MediaType.APPLICATION_JSON_VALUE)
public class MedicamentoResource {

	@Autowired
	private CidService service;

	@GetMapping
	@ApiOperation(value = "Retorna Todos os Medicamentos", notes = "", response = Cid.class)
	@PreAuthorize("hasAuthority('ROLE_PESQUISA_MEDICAMENTO')")
	public List<Cid> listar() {

		return service.listarTodos();

	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Consulta Medicamento por ID", notes = "", response = URI.class)
	@PreAuthorize("hasAuthority('ROLE_PESQUISA_MEDICAMENTO')")
	public Cid buscarPorId(@PathVariable Integer id) throws ObjectNotFoundException {

		Cid obj = service.buscarPorId(id);
		return obj;

	}

	@GetMapping(value = "/page")
	@ApiOperation(value = "Retorna Medicamento até 10 linhas por pagina", notes = "", response = Cid.class)
	@PreAuthorize("hasAuthority('ROLE_PESQUISA_MEDICAMENTO')")
	public ResponseEntity<Page<Cid>> findPage(@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "id") String orderBy) {

		Page<Cid> list = service.findPage(id, page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok().body(list);

	}

}
