package br.com.receitaseguraapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.enums.TipoInstituicao;
import br.com.receitaseguraapi.model.general.Endereco;
import br.com.receitaseguraapi.model.general.Instituicao;
import br.com.receitaseguraapi.repository.EnderecoRepository;
import br.com.receitaseguraapi.repository.InstituicaoRepository;
import br.com.receitaseguraapi.services.exceptions.DataIntegrityException;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;

@Service
public class InstituicaoService {

	@Autowired
	private InstituicaoRepository instituicaoRepository;

	@Autowired
	private EnderecoRepository enderecoRepository;

	public Instituicao find(Integer id) {

		Optional<Instituicao> obj = instituicaoRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Instituicao.class.getName()));

	}

	@Transactional
	public Instituicao insert(Instituicao obj) {

		obj.setId(null); 
		
		Instituicao inst = new Instituicao(obj.getCnpj(), obj.getRazaoSocial(), TipoInstituicao.CONSELHO);
		Endereco end = new Endereco(obj.getEndereco().getLogradouro(), obj.getEndereco().getBairro(), obj.getEndereco().getCep(), 
				obj.getEndereco().getCidade(), obj.getEndereco().getUf(), obj.getEndereco().getNumeroEndereco(), obj.getEndereco().getComplemento(), obj.getEndereco().getInstituicao(), null);

		inst.setEndereco(end);
		
		instituicaoRepository.save(inst);
		enderecoRepository.save(end);
				
		return inst;

	}

	public Instituicao update(Integer id, Instituicao instituicao) {

		Instituicao instituicaoSalvo = find(id);		
		BeanUtils.copyProperties(instituicao, instituicaoSalvo,  "id", "servicos");
		return instituicaoRepository.save(instituicaoSalvo);

	}

	public void delete(Integer id) {

		find(id);
		try {
			instituicaoRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel Excluir um Instituicao");
		}
	}

	public Page<Instituicao> findPage(String id, String razaoSocial, Integer page, Integer linesPerPage,
			String direction, String orderBy) {

		Pageable pageable = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);

		if (!id.equals("") && razaoSocial.equals("")) {
			return instituicaoRepository.findByApenasId(id, pageable);
		}
		if (!razaoSocial.equals("") && id.equals("")) {
			return instituicaoRepository.findByApenasRazaoSocial(razaoSocial, pageable);
		}


		return instituicaoRepository.findAll(pageable);
	}

	public List<Instituicao> listarTodas() {

		return instituicaoRepository.findAll();

	}

}
