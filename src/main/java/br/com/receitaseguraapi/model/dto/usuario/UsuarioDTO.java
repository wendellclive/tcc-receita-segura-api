package br.com.receitaseguraapi.model.dto.usuario;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.receitaseguraapi.model.enums.NivelDeAcesso;
import br.com.receitaseguraapi.model.enums.StatusUsuario;
import br.com.receitaseguraapi.model.general.Instituicao;
import br.com.receitaseguraapi.model.usuario.Usuario;
import br.com.receitaseguraapi.services.validation.UsuarioUpdate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
@UsuarioUpdate
public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDateTime dataCadastro;
	private String matricula;

	@NotEmpty(message = "Preeenchimento Obrigatório")
	@Length(min=5, max = 120, message = "O tamanho deve ter entre 5 e 120 caracteres")
	@Email(message="Email Inválido")
	private String email;
	
	@NotEmpty(message="Preeenchimento Obrigatório")
	@Length(min=5, max=200, message="O tamanho deve ser entre 5 e 120 caracteres")
	private String nomeUsuario;
	private StatusUsuario statusUsuario;
	private Integer tentativasDeAcesso;
	private NivelDeAcesso nivelDeAcesso;
	private Instituicao instituicao;

	public UsuarioDTO(Usuario obj) {

		id = obj.getId();
		dataCadastro = obj.getDataCadastro();
		matricula = obj.getMatricula();
		email = obj.getEmail();
		nomeUsuario = obj.getNomeUsuario();
		statusUsuario = obj.getStatusUsuario();
		tentativasDeAcesso = obj.getTentativasDeAcesso();
		nivelDeAcesso = obj.getNivelDeAcesso();
		instituicao = obj.getInstituicao();
	}
	
}
