package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.ENDERECOS;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.receitaseguraapi.model.dto.general.EnderecoDTO;
import br.com.receitaseguraapi.model.general.Endereco;
import br.com.receitaseguraapi.services.EnderecoService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Created by Wendell Clive Santos de Lira - Email: wendell.clive@gmail.com
 * Data: 14/09/2018
 */

// Incluir anotations para compor a camada End-Poin REST
@RestController
@Api(value = ENDERECOS, produces = MediaType.APPLICATION_JSON_VALUE, tags = {ConstantesTags.TAG_ENDERECO})
@RequestMapping(value = ENDERECOS, produces = MediaType.APPLICATION_JSON_VALUE)

public class EnderecosController {

	@Autowired
	private EnderecoService service;


	@ApiOperation(value="Busca Endereco por id", notes = "Endpoint para CONSULTAR Endereco por ID.", response = Endereco.class)
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ENDERECO')")
	public ResponseEntity<Endereco> find(@PathVariable Integer id) {

		Endereco obj = service.find(id);
		return ResponseEntity.ok().body(obj);

	}

	/*
	// Método para chamar Servico de inserir objeto
	@ApiOperation(value="Insere nova Endereco", notes = "Endpoint para INSERIR Endereço.", response = URI.class)
	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("hasAnyAuthority('ROLE_CADASTRA_ENDERECO')")
	public ResponseEntity<Void> insert(@RequestBody Endereco obj) {

		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();

	}
	@ApiOperation(value="Atualiza Endereco", notes = "Endpoint para ATUALIZAR Endereço.", response = ResponseEntity.class)
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_ENDERECO')")
	public ResponseEntity<Void> update(@RequestBody Endereco obj, @PathVariable Integer id) {

		obj.setId(id);
		obj = service.update(obj);

		return ResponseEntity.noContent().build();

	}
*/

	@ApiOperation(value="Remove Endereco", notes = "Endpoint para REMOVER Endereço.", response = ResponseEntity.class)
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Não é possível excluir uma Endereco que possui membros"),
			@ApiResponse(code = 404, message = "Código inexistente") })
	@PreAuthorize("hasAnyAuthority('ROLE_REMOVE_ENDERECO')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {

		service.delete(id);
		return ResponseEntity.noContent().build();

	}

	@ApiOperation(value="Retorna todos os Enderecos", notes = "Endpoint para CONSULTAR todos os Endereços.", response = EnderecoDTO.class)
	@RequestMapping(method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('ROLE_ADMINISTRADOR')")
	public ResponseEntity<List<EnderecoDTO>> findAll() {

		List<Endereco> list = service.findAll();
		List<EnderecoDTO> listDto = list.stream().map(obj -> new EnderecoDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);

	}

	@ApiOperation(value="Retorna Enderecos até 24 linhas por pagina", notes = "Endpoint para CONSULTAR Endereços paginado.", response = Endereco.class)
	@RequestMapping(value="/page", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ENDERECO')")
	public ResponseEntity<Page<Endereco>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="direction", defaultValue="DESC") String direction, 
			@RequestParam(value="orderBy", defaultValue="id") String orderBy) {
		Page<Endereco> list = service.findPage(page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok().body(list);
	}


	@ApiOperation(value="Busca Endereco por CEP", notes = "Endpoint para CONSULTAR Endereco por CEP.", response = Endereco.class)
	@RequestMapping(value="/cep/{cep}", method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ENDERECO')")
	public ResponseEntity<Endereco> find(@PathVariable String cep) throws ObjectNotFoundException {

		Endereco obj = service.findByCep(cep);
		return ResponseEntity.ok().body(obj);

	}

}
