package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoInstituicao {

	GESTOR_SISTEMA(1, "GestorSistema"),
	CONSELHO(2, "Conselho de Classe"),
	GOVERNO(3, "Governo");
	
	private int codigo;
	private String descricao;
	
	public static TipoInstituicao toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (TipoInstituicao x : TipoInstituicao.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
			
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
