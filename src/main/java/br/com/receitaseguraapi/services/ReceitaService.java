package br.com.receitaseguraapi.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.javafaker.Faker;

import br.com.receitaseguraapi.model.dto.general.ReceitaDTO;
import br.com.receitaseguraapi.model.enums.StatusDoc;
import br.com.receitaseguraapi.model.general.Documento;
import br.com.receitaseguraapi.model.general.Receita;
import br.com.receitaseguraapi.repository.DocumentoRepository;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;
import br.com.receitaseguraapi.utils.GeraAutorizacao;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ReceitaService {

	String imgLogo = "relatorios/bastao_asclepio.jpg";

	@Autowired
	private DocumentoRepository reposit;

	public Receita findById(Integer id) {

		Optional<Receita> obj = reposit.buscaReceitadoPorId(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Receita.class.getName()));

	}

	public Documento findByDocumentoNumero(String chave) {

		Optional<Documento> obj = reposit.findDocumentoNumero(chave);
		if (!obj.isPresent()) {
			return null;
		}
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Chave: " + chave + ", Tipo: " + Documento.class.getName()));

	}

	@Transactional
	public Receita insertReceita(Receita receita) throws ObjectNotFoundException {

		Faker faker = new Faker();

		receita.setId(null);
		receita.setStatusDoc(StatusDoc.VALIDO);
		
		if (receita.getAssinatura().isEmpty()) {
			
			throw new ObjectNotFoundException("Assinatura Não Realizada");
			
		} else {
			
			receita.setAssinatura(faker.crypto().md5());
			receita.setAutorizacao(GeraAutorizacao.nomeAleatorio(8));
			receita.setChaveAutenticacao(GeraAutorizacao.nomeAleatorio(5));
		}
		
		return reposit.save(receita);
	}

	public byte[] imprimirReceita(String autorizacao) throws JRException, IOException {

		InputStream imgInput = this.getClass().getClassLoader().getResourceAsStream(imgLogo);

		Map<String, Object> parametros = new HashMap<>();
		parametros.put("logo", imgInput);
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));

		InputStream inputStream;
		
		List<Receita> list = reposit.buscaReceitaPorAutorizacao(autorizacao);
		List<ReceitaDTO> dadosDTO = list.stream().map(obj -> new ReceitaDTO(obj)).collect(Collectors.toList());
		
		inputStream = this.getClass().getResourceAsStream("/relatorios/receita.jasper");

		JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros,
				new JRBeanCollectionDataSource(dadosDTO));

		byte[] exportado = JasperExportManager.exportReportToPdf(jasperPrint);

		imgInput.close();

		return exportado;

	}

	public Integer totalReceitas() {
		return reposit.totalReceitas();
	}

	public List<Documento> listarTodasReceitas() {
			return reposit.listarTodasReceitas();
	}

}
