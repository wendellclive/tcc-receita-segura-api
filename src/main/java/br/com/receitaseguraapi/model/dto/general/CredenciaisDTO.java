package br.com.receitaseguraapi.model.dto.general;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CredenciaisDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String matricula;
	private String senha;

}
