package br.com.receitaseguraapi.model.general;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.receitaseguraapi.model.enums.TipoPessoa;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@PrimaryKeyJoinColumn(referencedColumnName = "id")
@DiscriminatorValue("pessoa_id")
public class Medico extends Pessoa implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 3, max = 10)
	private String crm;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "especialidade_fk")
	private Especialidade especialidadeFk;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "lotacao_fk")
	private Instituicao lotacaoFk;

	public Medico(String nome, String cpf, String rg, TipoPessoa tipoPessoa, String crm, Especialidade especialidadeFk,
			Instituicao lotacaoFk) {
		super(nome, cpf, rg, tipoPessoa);
		this.crm = crm;
		this.especialidadeFk = especialidadeFk;
		this.lotacaoFk = lotacaoFk;
	}

}
