package br.com.receitaseguraapi.utils;

import br.com.receitaseguraapi.model.enums.PermissaoDeAcesso;
import br.com.receitaseguraapi.model.usuario.Usuario;

public class Permissoes {

	public Usuario adicionaPermissao(Usuario usuario) {
		
		int nivel = usuario.getNivelDeAcesso().getCodigo();
		
		// Administrador
		if (nivel == 10) {
			
			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_USUARIO);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICO);	
			
			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_INSTITUICAO);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_ESPECIALIDADE);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_PACIENTE);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_ATESTADO);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_RECEITA);
			
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICAMENTO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICAMENTO);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_CID);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_CID);

		}

		//Conselho
		if (nivel == 20) {
			
			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_USUARIO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_USUARIO);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICO);	
			
			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_INSTITUICAO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_INSTITUICAO);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_ESPECIALIDADE);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_ESPECIALIDADE);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_PACIENTE);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_ATESTADO);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICAMENTO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICAMENTO);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_CID);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_CID);

		}

		// Governo
		if (nivel == 30) {

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICO);	
			
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_PACIENTE);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_ATESTADO);


			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICAMENTO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICAMENTO);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_CID);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_CID);
			
		}

		// Profissional
		if (nivel == 40) {

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_PACIENTE);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_PACIENTE);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_ATESTADO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_ATESTADO);

			usuario.addPermissao(PermissaoDeAcesso.CADASTRA_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.REMOVE_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.EDITA_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_RECEITA);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_RECEITA);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_MEDICAMENTO);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_MEDICAMENTO);

			usuario.addPermissao(PermissaoDeAcesso.PESQUISA_CID);
			usuario.addPermissao(PermissaoDeAcesso.RELATORIO_CID);
			
		}
		 
		 usuario.setPermissoes(usuario.getPermissoes());
		
		 return usuario;
	}
}
