package br.com.receitaseguraapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.receitaseguraapi.model.general.Medicamento;

public interface MedicamentoRepository extends JpaRepository<Medicamento, Integer> {

}
