-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: receitasegura
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atestado`
--

DROP TABLE IF EXISTS `atestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atestado` (
  `dias_afastamento` int(11) DEFAULT NULL,
  `permite_informar_cid` bit(1) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `cid_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgt3jhh7q9imfeusho82387k14` (`cid_fk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atestado`
--

LOCK TABLES `atestado` WRITE;
/*!40000 ALTER TABLE `atestado` DISABLE KEYS */;
INSERT INTO `atestado` VALUES (15,_binary '',1,1),(15,_binary '',3,1),(15,_binary '',5,1),(15,_binary '',7,1),(15,_binary '',9,1),(15,_binary '',11,1),(15,_binary '',13,1),(15,_binary '',15,1),(15,_binary '',17,1),(15,_binary '',19,1),(15,_binary '',21,1),(15,_binary '',23,1),(15,_binary '',25,1),(15,_binary '',27,1),(15,_binary '',29,1);
/*!40000 ALTER TABLE `atestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cid`
--

DROP TABLE IF EXISTS `cid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `cid` varchar(3) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cid`
--

LOCK TABLES `cid` WRITE;
/*!40000 ALTER TABLE `cid` DISABLE KEYS */;
INSERT INTO `cid` VALUES (1,'2020-06-18 00:04:25','A00','Colera'),(2,'2020-06-18 00:04:25','A01','Febres tifoide e paratifoide'),(3,'2020-06-18 00:04:25','A02','Outr infecc p/Salmonella'),(4,'2020-06-18 00:04:25','A03','Shiguelose'),(5,'2020-06-18 00:04:25','A04','Outr infecc intestinais bacter'),(6,'2020-06-18 00:04:25','A05','Outr intox alimentares bacter NCOP'),(7,'2020-06-18 00:04:25','A06','Amebiase'),(8,'2020-06-18 00:04:25','A07','Outr doenc intestinais p/protozoarios'),(9,'2020-06-18 00:04:25','A08','Infecc intestinais virais outr e as NE'),(10,'2020-06-18 00:04:25','A09','Diarreia e gastroenterite orig infecc presum');
/*!40000 ALTER TABLE `cid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `tipo` varchar(31) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `assinatura` varchar(255) DEFAULT NULL,
  `autorizacao` varchar(255) DEFAULT NULL,
  `chave_autenticacao` varchar(255) DEFAULT NULL,
  `status_doc` int(11) DEFAULT NULL,
  `medico_fk` int(11) DEFAULT NULL,
  `paciene_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbslxbehpuno0j6ueppytwiqtg` (`medico_fk`),
  KEY `FKq29tr0roc7p64qx1gao0v0e3l` (`paciene_fk`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
INSERT INTO `documento` VALUES ('atestado',1,'2020-06-18 00:04:25','e01bae8b1c121a6a87dd00f4299830b6','DiYKie','741043b1f1e7a3b81bd9b03dbc68e308',1,1,2),('receita_simples',2,'2020-06-18 00:04:25','612cdbcd639b27579ede7630568ae9d5','iihkUn','95d32e61e041a97ffaaf67d7b10fb8f9',1,1,2),('atestado',3,'2020-06-18 00:04:25','961c1ffe49b83c13207b0d8ac3710696','1hrRmf','4175b9929d87ae9d07b71a330515a56e',1,3,4),('receita_simples',4,'2020-06-18 00:04:25','852189a82107b3507c60856534f3cfb','FD5t4g','11e1a6474f281c9807bb257480077708',1,3,4),('atestado',5,'2020-06-18 00:04:26','2f79b60cba2ad8be66aab71cba911bc0','YFrv2v','16a84cfdddc11a85554058683ef34836',1,5,6),('receita_simples',6,'2020-06-18 00:04:26','5d6106dd14fa765b9204a1cdf6e484ba','sL4JMh','e5b86da36167fbf23acff5cd89322a2b',1,5,6),('atestado',7,'2020-06-18 00:04:26','83bb2abdc8229cb9ff98bf07f35680fe','wEytPy','1be090645f648a40d98896dc86466611',1,7,8),('receita_simples',8,'2020-06-18 00:04:26','b8b5f728d522af37a743a8bfd125d0cc','EtNxmG','be0967319ed1da8c88917703ce58e13b',1,7,8),('atestado',9,'2020-06-18 00:04:26','dd1996eed3f06e198f765aa95960a1a8','yMVEHX','eb705b7475f5a5e3f5f793a6c35327da',1,9,10),('receita_simples',10,'2020-06-18 00:04:26','f9f06f42fd64d0fc811992c10293e7df','yEfqW7','a8898e74670b821f7c6e5db4e79ac1ae',1,9,10),('atestado',11,'2020-06-18 00:04:26','1546587c6902cd1d0a21af9de19dbbbe','35jebA','c7bff8f413dfb127d3a421b067d6cb63',1,11,12),('receita_simples',12,'2020-06-18 00:04:26','112fab1df7a048949606bcfe38b26a62','37YHbW','90194a88c8027bf76e3322847a6100a',1,11,12),('atestado',13,'2020-06-18 00:04:26','c7c6a9d3f841d05440032d765bc37d0f','6Po3Wr','ebfb4fc68acee9d8188e5790378ce213',1,13,14),('receita_simples',14,'2020-06-18 00:04:26','a25907d08e51bf6591101471059a629e','nQv997','375f9ad0c51ebb51ae1ac05af6846354',1,13,14),('atestado',15,'2020-06-18 00:04:26','b495ae814ab6503614c0925f924ac41b','b2Avmp','d84b426754d7bea789d621deef243c77',1,15,16),('receita_simples',16,'2020-06-18 00:04:26','a91ecd4341a940487bbc47a1cb58fa22','8Cukix','74e0be5759f12b04f3847caeed81d16a',1,15,16),('atestado',17,'2020-06-18 00:04:27','7d685be27be4e010ebc8ce25e47755bc','n9jyHM','e65fb0419e4a6bd1aa5ff7a3d62e25a7',1,17,18),('receita_simples',18,'2020-06-18 00:04:27','ebe9ba366311fbabcb369a6ba12fdbe','ojgTjj','924bab4ddb079e18fe6de00f1b9f91fd',1,17,18),('atestado',19,'2020-06-18 00:04:27','5f1e846341e1f010562f32fba1277ad5','AJoba5','454e9248f037353844abed717a286f82',1,19,20),('receita_simples',20,'2020-06-18 00:04:27','e357aba5c499258919a12e2720759f','TVDhkP','4b63be47848604f0346f824edf419da',1,19,20),('atestado',21,'2020-06-18 00:04:27','bdcb771d44bf422101c765f0a9d06668','PT8n7u','f278acd9d634f7489c0ea77df4ef874e',1,21,22),('receita_simples',22,'2020-06-18 00:04:27','a6aab93b3ae33326258394b573d89aa9','rxd93z','38e14fee8958deb474af2051d24b9a8c',1,21,22),('atestado',23,'2020-06-18 00:04:27','71c1d8b89f92731a154d9ae20ac50943','pVQTmm','41eb03acab6dc39d3de47693cce15bae',1,23,24),('receita_simples',24,'2020-06-18 00:04:27','61df60c5182f80881caa8fad0d7329d7','XFLaCH','edde7331b5ef7850e1107603c5e5236e',1,23,24),('atestado',25,'2020-06-18 00:04:27','39c2abffb1d56d17c6d374e10fafe993','YKPSy2','df434a2452c5a1d6c7dcc130c522fd7',1,25,26),('receita_simples',26,'2020-06-18 00:04:27','6bc22c689210642c0a9e230021af433d','3hfiKF','10fed049f1785e6f7521aefc91ba2117',1,25,26),('atestado',27,'2020-06-18 00:04:28','24e3aed94a58837a7033c4cbdfce2f53','LzN7NV','10e2946b6f8c4bbea0f384432fcc6957',1,27,28),('receita_simples',28,'2020-06-18 00:04:28','be00df3f7f6af8569d7bb99fc3ef847b','MGqYyz','b2e0ed3f57564864bef3444448b56b0b',1,27,28),('atestado',29,'2020-06-18 00:04:28','fda5c1f957bde03ac84657b2c4ea3e6a','5F2eW4','671b49f372c33e13755a61cff3c13f23',1,29,30),('receita_simples',30,'2020-06-18 00:04:28','1121e407065edd0440eaf39762edfdda','9NEC5L','d29904d9825d65237baa10800de91cc7',1,29,30);
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `endereco`
--

DROP TABLE IF EXISTS `endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `numero_endereco` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `endereco`
--

LOCK TABLES `endereco` WRITE;
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
INSERT INTO `endereco` VALUES (1,'2020-06-18 00:04:25','Japaratinga','54975-827','Retirolândia',NULL,'Alameda Ana Luiza','1200','Pernambuco'),(2,'2020-06-18 00:04:25','Nova Odessa','73581-005','Chapecó',NULL,'Avenida Carlos Eduardo','1200','Acre'),(3,'2020-06-18 00:04:25','Teixeira de Freitas','57965-422','Santana dos Montes',NULL,'Rodovia Maria Laura Castelo','1200','Paraná'),(4,'2020-06-18 00:04:25','Unaí','45961-480','Piedade dos Gerais',NULL,'Viela Rebeca','1200','Amapá'),(5,'2020-06-18 00:04:25','Teresópolis','69621-877','Assunção',NULL,'Travessa Mariah Alves','1200','Paraíba'),(6,'2020-06-18 00:04:25','Uruburetama','01555-547','Guaimbê',NULL,'Rua Maitê Ornelas','9406','Sergipe'),(7,'2020-06-18 00:04:25','Esperantina','73831-124','Caém',NULL,'Alameda Ana Clara','s/n','Amazonas'),(8,'2020-06-18 00:04:25','Coromandel','03686-654','Sapucaí-Mirim',NULL,'Viela Félix Pimenta','636','Santa Catarina'),(9,'2020-06-18 00:04:25','Atibaia','74240-816','Sabino',NULL,'Rodovia Kauê','9740','Alagoas'),(10,'2020-06-18 00:04:26','Ilópolis','35573-937','Lages',NULL,'Rodovia Luiz Felipe Simões','51106','Goiás'),(11,'2020-06-18 00:04:26','Taboão da Serra','97929-845','Cerro Grande',NULL,'Ponte Clara da Aldeia','1867','Sergipe'),(12,'2020-06-18 00:04:26','Faxinal do Soturno','57176-964','Arapuá',NULL,'Ponte Mirella Ribas','s/n','Acre'),(13,'2020-06-18 00:04:26','Horizonte','51597-852','São Julião',NULL,'Travessa Noah','45343','Pernambuco'),(14,'2020-06-18 00:04:26','Guararapes','96598-153','São Jorge do Patrocínio',NULL,'Avenida Marcela','569','Bahia'),(15,'2020-06-18 00:04:26','Carmópolis','11313-824','Tarumã',NULL,'Alameda Yasmin Bonfim','4048','Sergipe'),(16,'2020-06-18 00:04:26','Mendes Pimentel','67903-101','Conceição do Rio Verde',NULL,'Marginal Lavínia','s/n','Tocantins'),(17,'2020-06-18 00:04:26','São João de Pirabas','59473-746','Pedra Dourada',NULL,'Travessa Luiz','29764','Paraíba'),(18,'2020-06-18 00:04:26','Pedra Preta','45007-805','Maracaju',NULL,'Ponte Amanda da Cunha','0668','Rio de Janeiro'),(19,'2020-06-18 00:04:26','Panelas','07946-334','Fortaleza dos Valos',NULL,'Rua Luan Bernardes','330','Piauí'),(20,'2020-06-18 00:04:26','Amarante do Maranhão','46290-031','Porto de Pedras',NULL,'Viela Carlos Eduardo Lima','148','Bahia'),(21,'2020-06-18 00:04:26','Riachão','86475-589','Manoel Viana',NULL,'Rua Leonardo Galego','2939','Mato Grosso do Sul'),(22,'2020-06-18 00:04:27','Mata Roma','10324-386','Igaratinga',NULL,'Ponte Márcia','471','Alagoas'),(23,'2020-06-18 00:04:27','Altair','04750-673','Ouro Preto do Oeste',NULL,'Marginal Talita Ambrósio','0572','Mato Grosso do Sul'),(24,'2020-06-18 00:04:27','São Pedro da Cipa','77757-557','Barra Bonita',NULL,'Rua Emanuelly','53034','Mato Grosso'),(25,'2020-06-18 00:04:27','Carmo da Cachoeira','99664-818','Independência',NULL,'Alameda Bianca Sais','533','Rio de Janeiro'),(26,'2020-06-18 00:04:27','Areial','42932-401','Cristália',NULL,'Rodovia Isis','4613','Alagoas'),(27,'2020-06-18 00:04:27','São Desidério','30979-977','Buenos Aires',NULL,'Viela Hélio Marques','187','Pernambuco'),(28,'2020-06-18 00:04:27','Campinas do Piauí','86465-899','São José de Ubá',NULL,'Rua Maria Isis','685','Pernambuco'),(29,'2020-06-18 00:04:27','Jangada','18578-361','Vitória das Missões',NULL,'Alameda Feliciano do Prado','3640','Amapá'),(30,'2020-06-18 00:04:27','Joanésia','67057-341','Flor do Sertão',NULL,'Rua Isabelly','264','Distrito Federal'),(31,'2020-06-18 00:04:27','Jaci','32042-757','Malacacheta',NULL,'Viela Carolina Chaves','75154','Bahia'),(32,'2020-06-18 00:04:28','Araputanga','29766-611','Miguel Alves',NULL,'Rua Alice Lopes','167','Sergipe'),(33,'2020-06-18 00:04:28','Matinhos','59106-050','Herveiras',NULL,'Rua Luan','s/n','Ceará'),(34,'2020-06-18 00:04:28','Monte do Carmo','70462-979','Diadema',NULL,'Rua Isabela Pinto','489','Ceará'),(35,'2020-06-18 00:04:28','Santa Tereza de Goiás','55978-037','Bodó',NULL,'Rodovia Vitor','492','Rondônia');
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidade`
--

DROP TABLE IF EXISTS `especialidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especialidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `nome_especialidade` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidade`
--

LOCK TABLES `especialidade` WRITE;
/*!40000 ALTER TABLE `especialidade` DISABLE KEYS */;
INSERT INTO `especialidade` VALUES (1,'2020-06-18 00:04:25','Acupuntura'),(2,'2020-06-18 00:04:25','Alergia'),(3,'2020-06-18 00:04:25','Anestesiologia'),(4,'2020-06-18 00:04:25','Angiologia'),(5,'2020-06-18 00:04:25','Oncologia)'),(6,'2020-06-18 00:04:25','Cardiologia'),(7,'2020-06-18 00:04:25','Cirurgia Cardiovascular'),(8,'2020-06-18 00:04:25','Cirurgia da Mão'),(9,'2020-06-18 00:04:25','Cirurgia de Cabeça e Pescoço'),(10,'2020-06-18 00:04:25','Cirurgia do Aparelho Digestivo'),(11,'2020-06-18 00:04:25','Cirurgia Geral'),(12,'2020-06-18 00:04:25','Cirurgia Pediátrica'),(13,'2020-06-18 00:04:25','Cirurgia Plástica'),(14,'2020-06-18 00:04:25','Cirurgia Torácica'),(15,'2020-06-18 00:04:25','Cirurgia Vascular'),(16,'2020-06-18 00:04:25','Clínica Médica '),(17,'2020-06-18 00:04:25','Coloproctologia'),(18,'2020-06-18 00:04:25','Dermatologia'),(19,'2020-06-18 00:04:25','Endocrinologia'),(20,'2020-06-18 00:04:25','Endoscopia'),(21,'2020-06-18 00:04:25','Gastroenterologia'),(22,'2020-06-18 00:04:25','Genética Médica'),(23,'2020-06-18 00:04:25','Geriatria'),(24,'2020-06-18 00:04:25','Ginecologia e obstetrícia'),(25,'2020-06-18 00:04:25','Hematologia e Hemoterapia'),(26,'2020-06-18 00:04:25','Homeopatia'),(27,'2020-06-18 00:04:25','Infectologia'),(28,'2020-06-18 00:04:25','Mastologia'),(29,'2020-06-18 00:04:25','Medicina de Emergência'),(30,'2020-06-18 00:04:25','Medicina do Trabalho'),(31,'2020-06-18 00:04:25','Medicina do Tráfego'),(32,'2020-06-18 00:04:25','Medicina Esportiva'),(33,'2020-06-18 00:04:25','Medicina Intensiva'),(34,'2020-06-18 00:04:25','Medicina Nuclear'),(35,'2020-06-18 00:04:25','Medicina Preventiva'),(36,'2020-06-18 00:04:25','Nefrologia'),(37,'2020-06-18 00:04:25','Neurocirurgia'),(38,'2020-06-18 00:04:25','Neurologia'),(39,'2020-06-18 00:04:25','Nutrologia'),(40,'2020-06-18 00:04:25','Obstetrícia'),(41,'2020-06-18 00:04:25','Oftalmologia'),(42,'2020-06-18 00:04:25','Pediatria'),(43,'2020-06-18 00:04:25','Pneumologia'),(44,'2020-06-18 00:04:25','Psiquiatria'),(45,'2020-06-18 00:04:25','Radioterapia'),(46,'2020-06-18 00:04:25','Reumatologia'),(47,'2020-06-18 00:04:25','Urologia');
/*!40000 ALTER TABLE `especialidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instituicao`
--

DROP TABLE IF EXISTS `instituicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instituicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `razao_social` varchar(255) DEFAULT NULL,
  `tipo_instituicao` int(11) DEFAULT NULL,
  `instituicao_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4uyg3npor3049i17q9unkhe3g` (`instituicao_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instituicao`
--

LOCK TABLES `instituicao` WRITE;
/*!40000 ALTER TABLE `instituicao` DISABLE KEYS */;
INSERT INTO `instituicao` VALUES (1,'2020-06-18 00:04:25','76510882150033','TCC UNIPE ALUNOS',1,1),(2,'2020-06-18 00:04:25','24980545403030','CRM-PB',2,2),(3,'2020-06-18 00:04:25','27348944413667','CRM-PE',2,3),(4,'2020-06-18 00:04:25','45403951075218','CRM-RN',2,4),(5,'2020-06-18 00:04:25','52114534740815','CRM-AL',2,5);
/*!40000 ALTER TABLE `instituicao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicamento`
--

DROP TABLE IF EXISTS `medicamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `apresentacao` varchar(255) NOT NULL,
  `classe_terapeutica` varchar(255) NOT NULL,
  `cnpj` varchar(255) NOT NULL,
  `laboratorio` varchar(255) NOT NULL,
  `principio_ativo` varchar(255) NOT NULL,
  `produto` varchar(255) NOT NULL,
  `restricao_hospitalar` bit(1) NOT NULL,
  `tarja` varchar(255) NOT NULL,
  `tipo_produto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicamento`
--

LOCK TABLES `medicamento` WRITE;
/*!40000 ALTER TABLE `medicamento` DISABLE KEYS */;
INSERT INTO `medicamento` VALUES (1,'2020-06-18 00:04:25','250 MG PO LIOF INJ CT FA + SER DESC','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','AZITROMICINA','AZITROMICINA',_binary '','Tarja Vermelha','Biológicos'),(2,'2020-06-18 00:04:25','125 MG/ML SOL INJ CT 1 SER PREENCHIDA','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','BACTRIM','BACTRIM',_binary '\0','Tarja Vermelha','Biológicos'),(3,'2020-06-18 00:04:25','125 MG/ML SOL INJ CT 4 SER PREENCHIDA','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','ALMOXICILINA','ALMOXICILINA',_binary '\0','Tarja Vermelha','Biológicos'),(4,'2020-06-18 00:04:25','125 MG/ML SOL INJ CT 1 SER PREENCHIDA + DISPOSITIVO ULTRASAFE','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','CIPROFLOXACINO','CIPROFLOXACINO',_binary '\0','Tarja Vermelha','Biológicos'),(5,'2020-06-18 00:04:25','125 MG/ML SOL INJ CT 4 SER PREENCHIDA + DISPOSITIVO ULTRASAFE','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','DIPIRONA SÓDICA','DIPIRONA SÓDICA',_binary '\0','Tarja Vermelha','Biológicos'),(6,'2020-06-18 00:04:25','125 MG/ML SOL INJ CT 1 SER PREENC VD TRANS + DISPOSITIVO ULTRASAFE PASSIVE + EXTENSORES DE APOIO','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','PARACETAMOL','PARACETAMOL',_binary '\0','Tarja Vermelha','Biológicos'),(7,'2020-06-18 00:04:25','125 MG/ML SOL INJ CT 4 SER PREENC VD TRANS + DISPOSITIVO ULTRASAFE PASSIVE + EXTENSORES DE APOIO','M01C0 - AGENTES ANTI-REUMÁTICOS ESPECÍFICOS','56998982000107','BRISTOL-MYERS SQUIBB FARMACÊUTICA LTDA','CETOPROFENO','CETOPROFENO',_binary '\0','Tarja Vermelha','Biológicos'),(8,'2020-06-18 00:04:25','2 MG/ML SOL INJ CT FA VD INC X 5 ML','B01C3 - INIBIDORES DA AGREGAÇÃO PLAQUETÁRIA, ANTAGONISTAS GLICOPROTEÍNA IIB/IIIA','43940618000144','ELI LILLY DO BRASIL LTDA','DIMETICONA','DIMETICONA',_binary '','Tarja Vermelha','Biológicos'),(9,'2020-06-18 00:04:25','2 MG/ML SOL INJ CT FA VD INC X 5 ML','B01C3 - INIBIDORES DA AGREGAÇÃO PLAQUETÁRIA, ANTAGONISTAS GLICOPROTEÍNA IIB/IIIA','51780468000187','JANSSEN-CILAG FARMACÊUTICA LTDA','CECLOR','CECLOR',_binary '','Tarja Vermelha','Biológico Novo'),(10,'2020-06-18 00:04:25','100 MG COM CT 3 BL AL PVDC LEIT X 10','A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE','00923140000131','EMS SIGMA PHARMA LTDA','AAS','AAS',_binary '\0','Tarja Vermelha','Biológicos'),(11,'2020-06-18 00:04:25','100 MG COM CT BL AL/AL X 30','A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE','18459628000115','BAYER S.A.','ACICLOVIR','ACICLOVIR',_binary '\0','Tarja Vermelha','Biológicos'),(12,'2020-06-18 00:04:25','50 MG COM CT 3 BL AL PVDC LEIT X 10','A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE','00923140000131','EMS SIGMA PHARMA LTDA','SULFATO DE SALBUTAMOL','SULFATO DE SALBUTAMOL',_binary '\0','Tarja Vermelha','Biológicos'),(13,'2020-06-18 00:04:25','50 MG COM CT BL AL/AL X 30','A10L0 - ANTIDIABÉTICOS INIBIDORES ALFA-GLUCOSIDASE','18459628000115','BAYER S.A.','LORATADINA','LORATADINA',_binary '\0','Tarja Vermelha','Biológicos'),(14,'2020-06-18 00:04:25','5 MG/ML GEL OR CT FR PLAS INC X 120ML + COL MED','R05C0 - EXPECTORANTES','61190096000192','EUROFARMA LABORATÓRIOS S.A.','LEVOFLOXACINO','LEVOFLOXACINO',_binary '\0','Tarja Vermelha','Novo (Referência)'),(15,'2020-06-18 00:04:25','10 MG/ML XPE CT FR PLAS AMB X 120 ML + CP MED','R05C0 - EXPECTORANTES','02814497000107','CIMED INDÚSTRIA DE MEDICAMENTOS LTDA','IBUPROFENO','IBUPROFENO',_binary '\0','Tarja Vermelha','Genérico'),(16,'2020-06-18 00:04:25','10 MG/ML XPE CX 50 FR PLAS AMB X 120 ML + 50 CP MED (EMB HOSP)','R05C0 - EXPECTORANTES','02814497000107','CIMED INDÚSTRIA DE MEDICAMENTOS LTDA','HIDROXICLOROQUINA','HIDROXICLOROQUINA',_binary '','Tarja Vermelha','Genérico');
/*!40000 ALTER TABLE `medicamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medico`
--

DROP TABLE IF EXISTS `medico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medico` (
  `crm` varchar(10) NOT NULL,
  `id` int(11) NOT NULL,
  `especialidade_fk` int(11) NOT NULL,
  `lotacao_fk` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKebgs40e68f99q5hdnegg7bywp` (`especialidade_fk`),
  KEY `FKejf6khy2817fy11i602v320ak` (`lotacao_fk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medico`
--

LOCK TABLES `medico` WRITE;
/*!40000 ALTER TABLE `medico` DISABLE KEYS */;
INSERT INTO `medico` VALUES ('6930',1,1,1),('6308',3,1,1),('9312',5,1,1),('3456',7,1,1),('5241',9,1,1),('3404',11,1,1),('0196',13,1,1),('6636',15,1,1),('0651',17,1,1),('3551',19,1,1),('1919',21,1,1),('7705',23,1,1),('3402',25,1,1),('0783',27,1,1),('4112',29,1,1);
/*!40000 ALTER TABLE `medico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paciente`
--

DROP TABLE IF EXISTS `paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paciente` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paciente`
--

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;
INSERT INTO `paciente` VALUES (2),(4),(6),(8),(10),(12),(14),(16),(18),(20),(22),(24),(26),(28),(30);
/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissao_de_acesso`
--

DROP TABLE IF EXISTS `permissao_de_acesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissao_de_acesso` (
  `usuario_id` int(11) NOT NULL,
  `permissoes` int(11) DEFAULT NULL,
  KEY `FKiv147s30cutppg49p8xp5c6k4` (`usuario_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissao_de_acesso`
--

LOCK TABLES `permissao_de_acesso` WRITE;
/*!40000 ALTER TABLE `permissao_de_acesso` DISABLE KEYS */;
INSERT INTO `permissao_de_acesso` VALUES (1,70),(1,71),(1,72),(1,73),(1,10),(1,74),(1,11),(1,12),(1,13),(1,14),(1,83),(1,20),(1,84),(1,21),(1,22),(1,23),(1,24),(1,93),(1,30),(1,94),(1,31),(1,32),(1,33),(1,34),(1,100),(1,101),(1,102),(1,103),(1,40),(1,104),(1,41),(1,42),(1,43),(1,44),(1,50),(1,51),(1,52),(1,53),(1,54),(2,70),(2,71),(2,72),(2,73),(2,10),(2,74),(2,11),(2,12),(2,13),(2,14),(2,83),(2,20),(2,84),(2,21),(2,22),(2,23),(2,24),(2,93),(2,30),(2,94),(2,31),(2,32),(2,33),(2,34),(2,100),(2,101),(2,102),(2,103),(2,40),(2,104),(2,41),(2,42),(2,43),(2,44),(2,50),(2,51),(2,52),(2,53),(2,54),(3,70),(3,71),(3,72),(3,73),(3,10),(3,74),(3,11),(3,12),(3,13),(3,14),(3,83),(3,20),(3,84),(3,21),(3,22),(3,23),(3,24),(3,93),(3,30),(3,94),(3,31),(3,32),(3,33),(3,34),(3,100),(3,101),(3,102),(3,103),(3,40),(3,104),(3,41),(3,42),(3,43),(3,44),(3,50),(3,51),(3,52),(3,53),(3,54),(4,70),(4,71),(4,72),(4,73),(4,10),(4,74),(4,11),(4,12),(4,13),(4,14),(4,83),(4,20),(4,84),(4,21),(4,22),(4,23),(4,24),(4,93),(4,30),(4,94),(4,31),(4,32),(4,33),(4,34),(4,100),(4,101),(4,102),(4,103),(4,40),(4,104),(4,41),(4,42),(4,43),(4,44),(4,50),(4,51),(4,52),(4,53),(4,54),(5,70),(5,71),(5,72),(5,73),(5,10),(5,74),(5,11),(5,12),(5,13),(5,14),(5,83),(5,20),(5,84),(5,21),(5,22),(5,23),(5,24),(5,93),(5,30),(5,94),(5,31),(5,32),(5,33),(5,34),(5,100),(5,101),(5,102),(5,103),(5,40),(5,104),(5,41),(5,42),(5,43),(5,44),(5,50),(5,51),(5,52),(5,53),(5,54),(6,70),(6,71),(6,72),(6,73),(6,10),(6,74),(6,11),(6,12),(6,13),(6,14),(6,83),(6,20),(6,84),(6,21),(6,22),(6,23),(6,24),(6,93),(6,30),(6,94),(6,31),(6,32),(6,33),(6,34),(6,100),(6,101),(6,102),(6,103),(6,40),(6,104),(6,41),(6,42),(6,43),(6,44),(6,50),(6,51),(6,52),(6,53),(6,54),(7,70),(7,71),(7,72),(7,73),(7,10),(7,74),(7,11),(7,12),(7,13),(7,14),(7,83),(7,20),(7,84),(7,21),(7,22),(7,23),(7,24),(7,93),(7,30),(7,94),(7,31),(7,32),(7,33),(7,34),(7,100),(7,101),(7,102),(7,103),(7,40),(7,104),(7,41),(7,42),(7,43),(7,44),(7,50),(7,51),(7,52),(7,53),(7,54),(8,70),(8,71),(8,72),(8,73),(8,10),(8,74),(8,11),(8,12),(8,13),(8,14),(8,83),(8,20),(8,84),(8,21),(8,22),(8,23),(8,24),(8,93),(8,30),(8,94),(8,31),(8,32),(8,33),(8,34),(8,100),(8,101),(8,102),(8,103),(8,40),(8,104),(8,41),(8,42),(8,43),(8,44),(8,50),(8,51),(8,52),(8,53),(8,54),(9,70),(9,71),(9,72),(9,73),(9,10),(9,74),(9,11),(9,12),(9,13),(9,14),(9,83),(9,20),(9,84),(9,21),(9,22),(9,23),(9,24),(9,93),(9,30),(9,94),(9,31),(9,32),(9,33),(9,34),(9,100),(9,101),(9,102),(9,103),(9,40),(9,104),(9,41),(9,42),(9,43),(9,44),(9,50),(9,51),(9,52),(9,53),(9,54),(10,70),(10,71),(10,72),(10,73),(10,10),(10,74),(10,11),(10,12),(10,13),(10,14),(10,83),(10,20),(10,84),(10,21),(10,22),(10,23),(10,24),(10,93),(10,30),(10,94),(10,31),(10,32),(10,33),(10,34),(10,100),(10,101),(10,102),(10,103),(10,40),(10,104),(10,41),(10,42),(10,43),(10,44),(10,50),(10,51),(10,52),(10,53),(10,54),(11,70),(11,71),(11,72),(11,73),(11,10),(11,74),(11,11),(11,12),(11,13),(11,14),(11,83),(11,20),(11,84),(11,21),(11,22),(11,23),(11,24),(11,93),(11,30),(11,94),(11,31),(11,32),(11,33),(11,34),(11,100),(11,101),(11,102),(11,103),(11,40),(11,104),(11,41),(11,42),(11,43),(11,44),(11,50),(11,51),(11,52),(11,53),(11,54),(12,70),(12,71),(12,72),(12,73),(12,10),(12,74),(12,11),(12,12),(12,13),(12,14),(12,83),(12,20),(12,84),(12,21),(12,22),(12,23),(12,24),(12,93),(12,30),(12,94),(12,31),(12,32),(12,33),(12,34),(12,100),(12,101),(12,102),(12,103),(12,40),(12,104),(12,41),(12,42),(12,43),(12,44),(12,50),(12,51),(12,52),(12,53),(12,54),(13,70),(13,71),(13,72),(13,73),(13,10),(13,74),(13,11),(13,12),(13,13),(13,14),(13,83),(13,20),(13,84),(13,21),(13,22),(13,23),(13,24),(13,93),(13,30),(13,94),(13,31),(13,32),(13,33),(13,34),(13,100),(13,101),(13,102),(13,103),(13,40),(13,104),(13,41),(13,42),(13,43),(13,44),(13,50),(13,51),(13,52),(13,53),(13,54),(14,70),(14,71),(14,72),(14,73),(14,10),(14,74),(14,11),(14,12),(14,13),(14,14),(14,83),(14,20),(14,84),(14,21),(14,22),(14,23),(14,24),(14,93),(14,30),(14,94),(14,31),(14,32),(14,33),(14,34),(14,100),(14,101),(14,102),(14,103),(14,40),(14,104),(14,41),(14,42),(14,43),(14,44),(14,50),(14,51),(14,52),(14,53),(14,54),(15,70),(15,71),(15,72),(15,73),(15,10),(15,74),(15,11),(15,12),(15,13),(15,14),(15,83),(15,20),(15,84),(15,21),(15,22),(15,23),(15,24),(15,93),(15,30),(15,94),(15,31),(15,32),(15,33),(15,34),(15,100),(15,101),(15,102),(15,103),(15,40),(15,104),(15,41),(15,42),(15,43),(15,44),(15,50),(15,51),(15,52),(15,53),(15,54),(16,70),(16,71),(16,72),(16,73),(16,10),(16,74),(16,11),(16,12),(16,13),(16,14),(16,83),(16,20),(16,84),(16,21),(16,22),(16,23),(16,24),(16,93),(16,30),(16,94),(16,31),(16,32),(16,33),(16,34),(16,100),(16,101),(16,102),(16,103),(16,40),(16,104),(16,41),(16,42),(16,43),(16,44),(16,50),(16,51),(16,52),(16,53),(16,54),(17,70),(17,71),(17,72),(17,73),(17,10),(17,74),(17,11),(17,12),(17,13),(17,14),(17,83),(17,20),(17,84),(17,21),(17,22),(17,23),(17,24),(17,93),(17,30),(17,94),(17,31),(17,32),(17,33),(17,34),(17,100),(17,101),(17,102),(17,103),(17,40),(17,104),(17,41),(17,42),(17,43),(17,44),(17,50),(17,51),(17,52),(17,53),(17,54);
/*!40000 ALTER TABLE `permissao_de_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `rg` varchar(255) DEFAULT NULL,
  `tipo_pessoa` int(11) DEFAULT NULL,
  `pessoa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4fg9ohgo2cwk1ewodbcpdr51x` (`pessoa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,'2020-06-18 00:04:25','00520268648','Carolina dos Reis','2780876',1,6),(2,'2020-06-18 00:04:25','36781076901','Maria Isis Resende','2088366',2,7),(3,'2020-06-18 00:04:25','55986724873','Olívia Godinho','9513078',1,8),(4,'2020-06-18 00:04:25','72434279303','Benício Ramos Jr.','1618087',2,9),(5,'2020-06-18 00:04:26','43701378341','Marli Corrêa','8649834',1,10),(6,'2020-06-18 00:04:26','63880075751','Sr. Bryan Raia','6854265',2,11),(7,'2020-06-18 00:04:26','65052936413','Heitor Barreto Filho','5114238',1,12),(8,'2020-06-18 00:04:26','67235772373','Silas Cardoso Neto','1106877',2,13),(9,'2020-06-18 00:04:26','88371491551','Meire Pedroso','3088003',1,14),(10,'2020-06-18 00:04:26','86575746113','Núbia Brites','4715835',2,15),(11,'2020-06-18 00:04:26','87020870673','Clara Martim','6876188',1,16),(12,'2020-06-18 00:04:26','84518817740','Nicole da Costa','5712361',2,17),(13,'2020-06-18 00:04:26','73144531425','Srta. Lucas Gabriel Velasques','4290810',1,18),(14,'2020-06-18 00:04:26','81016538883','Matheus Guedes Jr.','0028144',2,19),(15,'2020-06-18 00:04:26','01172257025','Antônio das Neves','5018031',1,20),(16,'2020-06-18 00:04:26','58111052412','João Miguel da Aldeia','6747488',2,21),(17,'2020-06-18 00:04:27','11797602541','Rebeca Junqueira Jr.','3892056',1,22),(18,'2020-06-18 00:04:27','30065877102','Maria Valentina Longuinho Neto','4172104',2,23),(19,'2020-06-18 00:04:27','30757346374','Mathias Cardoso','6780422',1,24),(20,'2020-06-18 00:04:27','50148195618','Enzo Campos','3434746',2,25),(21,'2020-06-18 00:04:27','59885870761','Maria Fernanda Casqueira Neto','7501308',1,26),(22,'2020-06-18 00:04:27','75162328058','Dra. Cecília Siqueira','4215339',2,27),(23,'2020-06-18 00:04:27','63506037116','Dr. Salvador Simão','1228122',1,28),(24,'2020-06-18 00:04:27','37898632421','Francisca Rios','0278186',2,29),(25,'2020-06-18 00:04:27','53910773248','Tomás Tavares','2851462',1,30),(26,'2020-06-18 00:04:27','68651248134','Sr. Daniel Caldas','5198679',2,31),(27,'2020-06-18 00:04:28','86213677634','Thiago Ramalho','4645313',1,32),(28,'2020-06-18 00:04:28','14637001441','Marcia Moura','8022067',2,33),(29,'2020-06-18 00:04:28','11468321206','Víctor Moreno','5800853',1,34),(30,'2020-06-18 00:04:28','32524607279','Juliana Guterres','3173483',2,35);
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescricao`
--

DROP TABLE IF EXISTS `prescricao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescricao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `conduta` varchar(255) DEFAULT NULL,
  `medicamento_fk` int(11) NOT NULL,
  `prescricao_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdoqtab4g570wtdcbu7tcu37hd` (`medicamento_fk`),
  KEY `FK10bo9pp4ys1jj4demftevygp6` (`prescricao_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescricao`
--

LOCK TABLES `prescricao` WRITE;
/*!40000 ALTER TABLE `prescricao` DISABLE KEYS */;
INSERT INTO `prescricao` VALUES (1,'2020-06-18 00:04:25','Tomar 1caixa ao dia 12/12h',16,2),(2,'2020-06-18 00:04:25','Tomar 20ml ao dia 6/6h',1,2),(3,'2020-06-18 00:04:25','Tomar 5ml ao dia 12/12h',8,2),(4,'2020-06-18 00:04:25','Tomar 1caixa ao dia 12/12h',16,4),(5,'2020-06-18 00:04:25','Tomar 20ml ao dia 6/6h',1,4),(6,'2020-06-18 00:04:25','Tomar 5ml ao dia 12/12h',8,4),(7,'2020-06-18 00:04:26','Tomar 1caixa ao dia 12/12h',16,6),(8,'2020-06-18 00:04:26','Tomar 20ml ao dia 6/6h',1,6),(9,'2020-06-18 00:04:26','Tomar 5ml ao dia 12/12h',8,6),(10,'2020-06-18 00:04:26','Tomar 1caixa ao dia 12/12h',16,8),(11,'2020-06-18 00:04:26','Tomar 20ml ao dia 6/6h',1,8),(12,'2020-06-18 00:04:26','Tomar 5ml ao dia 12/12h',8,8),(13,'2020-06-18 00:04:26','Tomar 1caixa ao dia 12/12h',16,10),(14,'2020-06-18 00:04:26','Tomar 20ml ao dia 6/6h',1,10),(15,'2020-06-18 00:04:26','Tomar 5ml ao dia 12/12h',8,10),(16,'2020-06-18 00:04:26','Tomar 1caixa ao dia 12/12h',16,12),(17,'2020-06-18 00:04:26','Tomar 20ml ao dia 6/6h',1,12),(18,'2020-06-18 00:04:26','Tomar 5ml ao dia 12/12h',8,12),(19,'2020-06-18 00:04:26','Tomar 1caixa ao dia 12/12h',16,14),(20,'2020-06-18 00:04:26','Tomar 20ml ao dia 6/6h',1,14),(21,'2020-06-18 00:04:26','Tomar 5ml ao dia 12/12h',8,14),(22,'2020-06-18 00:04:26','Tomar 1caixa ao dia 12/12h',16,16),(23,'2020-06-18 00:04:26','Tomar 20ml ao dia 6/6h',1,16),(24,'2020-06-18 00:04:26','Tomar 5ml ao dia 12/12h',8,16),(25,'2020-06-18 00:04:27','Tomar 1caixa ao dia 12/12h',16,18),(26,'2020-06-18 00:04:27','Tomar 20ml ao dia 6/6h',1,18),(27,'2020-06-18 00:04:27','Tomar 5ml ao dia 12/12h',8,18),(28,'2020-06-18 00:04:27','Tomar 1caixa ao dia 12/12h',16,20),(29,'2020-06-18 00:04:27','Tomar 20ml ao dia 6/6h',1,20),(30,'2020-06-18 00:04:27','Tomar 5ml ao dia 12/12h',8,20),(31,'2020-06-18 00:04:27','Tomar 1caixa ao dia 12/12h',16,22),(32,'2020-06-18 00:04:27','Tomar 20ml ao dia 6/6h',1,22),(33,'2020-06-18 00:04:27','Tomar 5ml ao dia 12/12h',8,22),(34,'2020-06-18 00:04:27','Tomar 1caixa ao dia 12/12h',16,24),(35,'2020-06-18 00:04:27','Tomar 20ml ao dia 6/6h',1,24),(36,'2020-06-18 00:04:27','Tomar 5ml ao dia 12/12h',8,24),(37,'2020-06-18 00:04:27','Tomar 1caixa ao dia 12/12h',16,26),(38,'2020-06-18 00:04:27','Tomar 20ml ao dia 6/6h',1,26),(39,'2020-06-18 00:04:27','Tomar 5ml ao dia 12/12h',8,26),(40,'2020-06-18 00:04:28','Tomar 1caixa ao dia 12/12h',16,28),(41,'2020-06-18 00:04:28','Tomar 20ml ao dia 6/6h',1,28),(42,'2020-06-18 00:04:28','Tomar 5ml ao dia 12/12h',8,28),(43,'2020-06-18 00:04:28','Tomar 1caixa ao dia 12/12h',16,30),(44,'2020-06-18 00:04:28','Tomar 20ml ao dia 6/6h',1,30),(45,'2020-06-18 00:04:28','Tomar 5ml ao dia 12/12h',8,30);
/*!40000 ALTER TABLE `prescricao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receita`
--

DROP TABLE IF EXISTS `receita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receita` (
  `data_validade` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receita`
--

LOCK TABLES `receita` WRITE;
/*!40000 ALTER TABLE `receita` DISABLE KEYS */;
INSERT INTO `receita` VALUES ('2020-08-17 00:04:25',2),('2020-08-17 00:04:25',4),('2020-08-17 00:04:26',6),('2020-08-17 00:04:26',8),('2020-08-17 00:04:26',10),('2020-08-17 00:04:26',12),('2020-08-17 00:04:26',14),('2020-08-17 00:04:26',16),('2020-08-17 00:04:27',18),('2020-08-17 00:04:27',20),('2020-08-17 00:04:27',22),('2020-08-17 00:04:27',24),('2020-08-17 00:04:27',26),('2020-08-17 00:04:28',28),('2020-08-17 00:04:28',30);
/*!40000 ALTER TABLE `receita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_cadastro` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `matricula` varchar(255) NOT NULL,
  `nivel_de_acesso` int(11) DEFAULT NULL,
  `nome_usuario` varchar(255) DEFAULT NULL,
  `primeiro_acesso` bit(1) NOT NULL,
  `password` varchar(250) DEFAULT NULL,
  `status_usuario` int(11) DEFAULT NULL,
  `tentativas_de_acesso` int(11) DEFAULT NULL,
  `tipo_usuario` int(11) DEFAULT NULL,
  `instituicao_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6r3pgk87k0cmatekasqghpka6` (`matricula`),
  KEY `FKgcyda9ugb6f0qqdu02pds6g95` (`instituicao_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'2020-06-18 00:04:25','fulano@gmail.com','wendell',10,'WENDELL CLIVE',_binary '','$2a$10$6JUO/68Rkj8Eq4ChvR/v2O3pN1j8zbXdB.DlxnQe9C/daZQqYBIs.',1,0,1,1),(2,'2020-06-18 00:04:25','fulano@gmail.com','israel',10,'ISRAEL ARAUJO',_binary '','$2a$10$54qIH7p4iE45tA1bpFKI.uJEEcTLxrGB8Ro3Y4/ROQfbsRdpKwb96',1,0,1,1),(3,'2020-06-18 00:04:25','fulano@gmail.com','1',10,'Carolina dos Reis',_binary '','$2a$10$YuSnUx7bvEOScmsiqLCXku0JvPPlkjBi8yGnnXYdSeSjQuSmOdDuu',1,0,1,1),(4,'2020-06-18 00:04:25','fulano@gmail.com','3',10,'Olívia Godinho',_binary '','$2a$10$C1fPs5GHzqsdeq5dcLoQu.RpYLLXRLfWVqu.ReMjmsVYOaiS4Bl0i',1,0,1,1),(5,'2020-06-18 00:04:26','fulano@gmail.com','5',10,'Marli Corrêa',_binary '','$2a$10$Mh1wDTD4ZdY1kAjnHTPYcevkGCixCXE2Vzyp/v.IoySLRVIJiplF6',1,0,1,1),(6,'2020-06-18 00:04:26','fulano@gmail.com','7',10,'Heitor Barreto Filho',_binary '','$2a$10$81hCLLs6Wfrw2I1u8tE.OekUFGi4cdetr9sAdUDkn/ZxMpPkoTm3.',1,0,1,1),(7,'2020-06-18 00:04:26','fulano@gmail.com','9',10,'Meire Pedroso',_binary '','$2a$10$r83pLMhzQbRWwUZsR2eVBep34eVAeBy2o0pmIwwOC17wVA3A/zdqq',1,0,1,1),(8,'2020-06-18 00:04:26','fulano@gmail.com','11',10,'Clara Martim',_binary '','$2a$10$.eDjj8I9I5coeyiJXI1Qr.M9DqwgVVvT8neoy7mCxybP7GB2rjLie',1,0,1,1),(9,'2020-06-18 00:04:26','fulano@gmail.com','13',10,'Srta. Lucas Gabriel Velasques',_binary '','$2a$10$vBG938ckkjAervAexn7EhuU9XltWt3Pb.M/QF/g0rupjJ92X7ioCy',1,0,1,1),(10,'2020-06-18 00:04:27','fulano@gmail.com','15',10,'Antônio das Neves',_binary '','$2a$10$U1dg4Rj5dIFCA8GbX2VDa.NiagHGtfPd7Xa.eeZM5X6aGsiODuuJa',1,0,1,1),(11,'2020-06-18 00:04:27','fulano@gmail.com','17',10,'Rebeca Junqueira Jr.',_binary '','$2a$10$gEVGAgtbzfsr18Y/1BTVvOJisP/aGQQ0LT.NWwvMT7CA9BGrVs5wO',1,0,1,1),(12,'2020-06-18 00:04:27','fulano@gmail.com','19',10,'Mathias Cardoso',_binary '','$2a$10$wSPprSVHRCJzdnbcka/15eDqIQZd2pUtow3Ie9UVRy3AVdyjJuHcO',1,0,1,1),(13,'2020-06-18 00:04:27','fulano@gmail.com','21',10,'Maria Fernanda Casqueira Neto',_binary '','$2a$10$AVS2IvEYNhKpei4O3mGXJOFa54Aq5EqjmCGQBPpEcsWqpYeum.26i',1,0,1,1),(14,'2020-06-18 00:04:27','fulano@gmail.com','23',10,'Dr. Salvador Simão',_binary '','$2a$10$ILS.zmrzCi.6bN689ATIkuv48uUD1oN.wCN9JUZBtRCNpalbNijoq',1,0,1,1),(15,'2020-06-18 00:04:28','fulano@gmail.com','25',10,'Tomás Tavares',_binary '','$2a$10$8t4YUEt4RPbI1NBuGbk3aus3xkYIg0xFhH6TXKzuNKngrFZInN3rS',1,0,1,1),(16,'2020-06-18 00:04:28','fulano@gmail.com','27',10,'Thiago Ramalho',_binary '','$2a$10$NWTPV7IlDo1bnxDfROBiNeaxKIfpTXpzdLy4E.IT/cQ9GUN9X6jYW',1,0,1,1),(17,'2020-06-18 00:04:28','fulano@gmail.com','29',10,'Víctor Moreno',_binary '','$2a$10$lqd7Od/u1wlw2k3xsVzkXOlZmaZ3ITYLPYA2lQe3dCglmEgUXLbZ6',1,0,1,1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-18  0:05:56
