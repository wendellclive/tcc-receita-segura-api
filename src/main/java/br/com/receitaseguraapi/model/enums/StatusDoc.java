package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusDoc {

	VALIDO(1, "Válido"),
	CANCELADO(2, "Cancelado");
	
	private int codigo;
	private String descricao;
	
	public static StatusDoc toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (StatusDoc x : StatusDoc.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
			
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
