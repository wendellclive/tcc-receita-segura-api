package br.com.receitaseguraapi.model.dto.usuario;

import java.io.Serializable;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import br.com.receitaseguraapi.model.general.Sequencial;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@Getter
@Setter
public class UsuarioNewDTO extends Sequencial implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String matricula;
	@Length(min=1, message = "O tamanho deve ser entre 5 e 20 caracteres") 	
	@Column(name = "password", nullable = false, length = 250)
	private String senha;
	
	@NotEmpty(message = "Preeenchimento Obrigatório")
	@Length(min=5, max = 120, message = "O tamanho deve ter entre 5 e 120 caracteres")
	@Email(message="Email Inválido")
	private String email;
	
	@NotEmpty(message="Preeenchimento Obrigatório")
	@Length(min=5, max=200, message="O tamanho deve ser entre 5 e 120 caracteres")
	private String nomeUsuario;
	private Integer statusUsuario;
	private Integer tentativasDeAcesso;
	private Integer nivelDeAcesso;
	
}
