package br.com.receitaseguraapi.security;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.receitaseguraapi.model.enums.NivelDeAcesso;
import br.com.receitaseguraapi.model.enums.PermissaoDeAcesso;
import br.com.receitaseguraapi.model.enums.TipoUsuario;
import br.com.receitaseguraapi.model.general.Instituicao;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserSS implements UserDetails {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String matricula;
	private String senha;
	private String nomeUsuario;
	private Integer nivelDeAcesso;
	private boolean primeiroAcesso;
	private Collection<? extends GrantedAuthority> authorities;
	private Instituicao instituicao;
	private Integer tipoUsuario;

	public UserSS(Integer id, String matricula, String senha, String nomeUsuario, 
			NivelDeAcesso nivelDeAcesso, boolean primeiroAcesso, Set<PermissaoDeAcesso> permissoes,
			Instituicao instituicao, TipoUsuario tipoUsuario) {
		super();
		this.id = id;
		this.matricula = matricula;
		this.senha = senha;
		this.nomeUsuario = nomeUsuario;
		this.nivelDeAcesso = nivelDeAcesso.getCodigo();
		this.primeiroAcesso = primeiroAcesso;
		this.authorities = permissoes.stream().map(x -> new SimpleGrantedAuthority(x.getDescricao())).collect(Collectors.toList());
		this.instituicao = instituicao;
		this.tipoUsuario = tipoUsuario.getCodigo();
	}

	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return matricula;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public boolean hasHole(PermissaoDeAcesso permissaoDeAcesso) {
		return getAuthorities().contains(new SimpleGrantedAuthority(permissaoDeAcesso.getDescricao()));
	}

	public Integer getId() {
		return id;
	}
	
	public Integer getNivelDeAcesso() {
		return nivelDeAcesso;
	}

	public boolean getPrimeiroAcesso() {
		return primeiroAcesso;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public Instituicao getInstituicao() {
		return instituicao;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}
}
