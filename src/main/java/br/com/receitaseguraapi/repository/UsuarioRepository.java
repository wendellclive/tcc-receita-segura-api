package br.com.receitaseguraapi.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.usuario.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Transactional(readOnly=true)
	public Optional<Usuario> findByMatricula(String matricula);
	
	@Transactional(readOnly=true)
	@Query(value = "select * from receitasegura.usuario u where u.matricula = :matricula", nativeQuery = true)
	public Page<Usuario> findByApenasMatricula(@Param("matricula") String matricula, Pageable pageable);
	
	@Transactional(readOnly=true)
	@Query(value = "select * from receitasegura.usuario u where u.nome_usuario like %:nomeUsuario%", nativeQuery = true)
	public Page<Usuario> findByApenasNomeUsuario(@Param("nomeUsuario") String nomeUsuario, Pageable pageable);

	@Transactional(readOnly=true)
	@Query(value = "select * from receitasegura.usuario u where u.matricula like %:matricula% and u.nome_usuario like %:nomeUsuario%", nativeQuery = true)
	public Page<Usuario> findByMatriculaAndNomeUsuario(@Param("matricula") String matricula, @Param("nomeUsuario") String nomeUsuario, Pageable pageable);

}
