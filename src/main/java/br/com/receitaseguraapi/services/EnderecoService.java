package br.com.receitaseguraapi.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import br.com.receitaseguraapi.model.general.Endereco;
import br.com.receitaseguraapi.repository.EnderecoRepository;
import br.com.receitaseguraapi.services.exceptions.DataIntegrityException;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository reposit;
	
	public Endereco find(Integer id) {

		Optional<Endereco> obj = reposit.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Endereco.class.getName()));

	}

	public Endereco findByCep(String cep) {

		Optional<Endereco> obj = reposit.findByCep(cep);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + cep + ", Tipo: " + Endereco.class.getName()));

	}

	@Transactional
	public Endereco insert(Endereco obj) {

		obj.setId(null); // faz o método entender que se não houver ID então é uma alteração
		return reposit.save(obj);

	}

	public Endereco update(Endereco obj) {

		Endereco newObj = find(obj.getId());
		updateData(newObj, obj);
		return reposit.save(newObj);

	}

	public void delete(Integer id) {

		find(id);
		try {
			reposit.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel Excluir um Endereco");
		}
	}

	public List<Endereco> findAll() {
		return reposit.findAll();
	}

	public Page<Endereco> findPage(Integer page, Integer linesPerPage, String direction, String orderBy) {

		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return reposit.findAll(pageRequest);
	}

	private void updateData(Endereco newObj, Endereco obj) {

		newObj.setLogradouro(obj.getLogradouro());
		newObj.setBairro(obj.getBairro());
		newObj.setCep(obj.getCep());
		newObj.setCidade(obj.getCidade());
	}
	
}
