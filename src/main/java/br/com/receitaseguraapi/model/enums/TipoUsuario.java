package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoUsuario {

	ADMINISTRADOR(1, "Conselho de Classe"),
	GESTOR(2, "Conselho de Classe"),
	FISCAL(3, "Governo"),
	PROFISSIONAL(4, "Profissional");
	
	private int codigo;
	private String descricao;
	
	public static TipoUsuario toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (TipoUsuario x : TipoUsuario.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
			
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
