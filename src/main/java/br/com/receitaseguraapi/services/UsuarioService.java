package br.com.receitaseguraapi.services;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.receitaseguraapi.model.enums.StatusUsuario;
import br.com.receitaseguraapi.model.usuario.Usuario;
import br.com.receitaseguraapi.repository.UsuarioRepository;
import br.com.receitaseguraapi.services.exceptions.DataIntegrityException;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;
import br.com.receitaseguraapi.utils.Permissoes;

@Service
public class UsuarioService {

	@Autowired
	private PasswordEncoder pe;

	@Autowired
	private UsuarioRepository reposit;

	public Usuario find(Integer id) throws ObjectNotFoundException {

		Optional<Usuario> obj = reposit.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Usuario.class.getName()));

	}

	@Transactional
	public Usuario insert(Usuario usuario) {

		usuario.setId(null); // faz o método entender que se não houver ID então é uma alteração
		usuario.setSenha(pe.encode(usuario.getMatricula()));
		usuario.setPrimeiroAcesso(true);

		Permissoes permissoes = new Permissoes();
		
		usuario = permissoes.adicionaPermissao(usuario);
				
		return reposit.save(usuario);

	}

	public Usuario update(Integer id, Usuario usuario) {

		Usuario usuarioSalvo = find(id);
		
		usuario.setDataCadastro(usuarioSalvo.getDataCadastro());
		usuario.setSenha(usuarioSalvo.getSenha());	
		
		Permissoes permissoes = new Permissoes();
		usuario = permissoes.adicionaPermissao(usuario);
		
		BeanUtils.copyProperties(usuario, usuarioSalvo);
		
		return reposit.save(usuarioSalvo);
	}

	public void delete(Integer id) {

		find(id);
		try {
			reposit.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel Excluir um Usuario");
		}
	}

	public Usuario findByMatricula(String matricula) {

		Optional<Usuario> obj = reposit.findByMatricula(matricula);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + matricula + ", Tipo: " + Usuario.class.getName()));

	}

	public Page<Usuario> findPage(String matricula, String nomeUsuario, Integer page, Integer linesPerPage,
			String direction, String orderBy) {

		Pageable pageable = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);

		if (!matricula.equals("") && nomeUsuario.equals("")) {
			
			return reposit.findByApenasMatricula(matricula, pageable);
			
		} 
		
		if (!nomeUsuario.equals("") && matricula.equals("")) {
			
			return reposit.findByApenasNomeUsuario(nomeUsuario, pageable);
			
		} 
		
		if (!nomeUsuario.equals("") && !matricula.equals("")) {
			
			return reposit.findByMatriculaAndNomeUsuario(matricula, nomeUsuario, pageable);
			
		}

		return reposit.findAll(pageable);
		
	}

	public void atualizarPropriedadeAtivo(Integer id, @Valid StatusUsuario statusUsuario) {

		Usuario usuarioSalvo = find(id);
		usuarioSalvo.setStatusUsuario(statusUsuario);;
		reposit.save(usuarioSalvo);
		
	}

}
