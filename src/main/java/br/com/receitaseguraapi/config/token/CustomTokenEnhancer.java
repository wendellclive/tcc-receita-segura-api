package br.com.receitaseguraapi.config.token;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import br.com.receitaseguraapi.security.UserSS;

public class CustomTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
	
		UserSS usuarioSistema = (UserSS) authentication.getPrincipal();
		
		Map<String, Object> addInfo = new HashMap<>();
		addInfo.put("id", usuarioSistema.getId());
		addInfo.put("matricula", usuarioSistema.getUsername());
		addInfo.put("nome", usuarioSistema.getNomeUsuario());
		addInfo.put("niveldeacesso", usuarioSistema.getNivelDeAcesso());
		addInfo.put("primeiroacesso", usuarioSistema.getPrimeiroAcesso());
		addInfo.put("tipousuario", usuarioSistema.getTipoUsuario());
		addInfo.put("instituicao", usuarioSistema.getInstituicao().getId());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);
		return accessToken;
	}
}
