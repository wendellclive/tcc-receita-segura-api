package br.com.receitaseguraapi.resource.general;

import static br.com.receitaseguraapi.utils.ConstantesPaths.ATESTADOS;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.receitaseguraapi.model.general.Atestado;
import br.com.receitaseguraapi.model.general.Documento;
import br.com.receitaseguraapi.services.AtestadoService;
import br.com.receitaseguraapi.services.exceptions.ObjectNotFoundException;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = ATESTADOS, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_ATESTADO })
@RequestMapping(value = ATESTADOS, produces = MediaType.APPLICATION_JSON_VALUE)
public class AtestadoController {

	@Autowired
	private AtestadoService service;

	@ApiOperation(value = "Busca Atestado Beneficiario por id", notes = "Endpoint para CONSULTAR Atestado Beneficiario por ID.", response = Atestado.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ATESTADO')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Atestado> buscaAtestado(@PathVariable Integer id) {

		Atestado obj = service.findById(id);
		return ResponseEntity.ok().body(obj);

	}

	@ApiOperation(value = "Insere Atestado Beneficiario", notes = "Endpoint para INSERIR Atestado Beneficiario.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_ADMINISTRADOR', 'ROLE_CADASTRA_ATESTADO')")
	@PostMapping
	public ResponseEntity<Atestado> inserir(@RequestBody Atestado documento) throws ObjectNotFoundException {

		Atestado documentoSalvo = service.insertAtestado(documento);
		
		return ResponseEntity.ok().body(documentoSalvo);

	}
	
	@ApiOperation(value = "Impressão de Atestado Beneficiario de Cadastro Individual do TitularDTO", notes = "Endpoint para IMPRIMIR Atestado Beneficiario Individual do TitularDTO.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ATESTADO')")
	@GetMapping(value = "/imprimirAtestado/{autorizacao}")
	public ResponseEntity<byte[]> imprimirAtestado(@PathVariable String autorizacao) throws Exception {

		byte[] relatorio = service.imprimirAtestado(autorizacao);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);

	}
	
	@ApiOperation(value = "Impressão de Atestado Beneficiario de Cadastro Individual do TitularDTO", notes = "Endpoint para IMPRIMIR Atestado Beneficiario Individual do TitularDTO.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ATESTADO')")
	@GetMapping(value = "/imprimirAtestadoId/{id}")
	public ResponseEntity<byte[]> imprimirAtestadoId(@PathVariable int id) throws Exception {

		byte[] relatorio = service.imprimirAtestadoId(id);

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);

	}

	@ApiOperation(value = "Retorna total de Atestados", notes = "Endpoint para Retornar as total de Atestados.", response = Atestado.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ATESTADO')")
	@GetMapping(value = "/totalAtestados")
	public ResponseEntity<Integer> totalAtestados() {

		Integer total = service.totalAtestados();

		return ResponseEntity.ok().body(total);

	}
	
	@ApiOperation(value = "Retorna todos os Atestados", notes = "Endpoint para CONSULTAR todos os Atestados.", response = Atestado.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_ATESTADO')")
	@GetMapping
	public ResponseEntity<List<Documento>> findAll() {

		List<Documento> list = service.listarTodosAtestados();

		return ResponseEntity.ok().body(list);

	}

}
