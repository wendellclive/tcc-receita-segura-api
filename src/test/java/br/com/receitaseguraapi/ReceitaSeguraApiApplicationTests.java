package br.com.receitaseguraapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.receitaseguraapi.ReceitaSeguraApiApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ReceitaSeguraApiApplication.class)
@TestPropertySource(locations="classpath:application.properties")
public class ReceitaSeguraApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
