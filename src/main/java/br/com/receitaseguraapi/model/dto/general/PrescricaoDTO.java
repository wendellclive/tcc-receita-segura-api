package br.com.receitaseguraapi.model.dto.general;

import java.io.Serializable;
import java.util.Date;

import br.com.receitaseguraapi.model.general.Prescricao;
import br.com.receitaseguraapi.utils.Conversor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrescricaoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Date dataCadastro;
	private String produto;
	private String conduta;

	
	public PrescricaoDTO(Prescricao obj) {

		id = obj.getId();
		dataCadastro = Conversor.converterParaData(obj.getDataCadastro());
		produto = obj.getMedicamentoFk().getProduto();
		conduta = obj.getConduta();
		
	}

}
