package br.com.receitaseguraapi.config.property;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("receitaseguraapi")
public class ReceitaSeguraApiProperty {

	@Getter
	@Setter
	@Value("${origem.permitida}")
	private String originPermitida;
	
	@Getter
	private final Seguranca seguranca = new Seguranca();

	@Getter
	@Setter
	public static class Seguranca {

		private boolean enableHttps;

	}
}
