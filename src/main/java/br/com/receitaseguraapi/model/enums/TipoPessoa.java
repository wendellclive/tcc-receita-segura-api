package br.com.receitaseguraapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPessoa {

	MEDICO(1, "Profissional"),
	PACIENTE(2, "Paciente");
	
	private int codigo;
	private String descricao;
	
	public static TipoPessoa toEnum(Integer codigo) {
		
		if (codigo == null) {
			return null;
		}
		for (TipoPessoa x : TipoPessoa.values()) {
			if(codigo.equals(x.getCodigo())) {
				return x;
			}
			
		}
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
