package br.com.receitaseguraapi.resource.usuario;

import static br.com.receitaseguraapi.utils.ConstantesPaths.USUARIOS;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.receitaseguraapi.model.dto.usuario.UsuarioDTO;
import br.com.receitaseguraapi.model.enums.StatusUsuario;
import br.com.receitaseguraapi.model.usuario.Usuario;
import br.com.receitaseguraapi.services.UsuarioService;
import br.com.receitaseguraapi.utils.ConstantesTags;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Created by Wendell Clive Santos de Lira - Email: wendell.clive@gmail.com
 * Data: 14/09/2018
 */

// Incluir anotations para compor a camada End-Poin REST
@RestController
@Api(value = USUARIOS, produces = MediaType.APPLICATION_JSON_VALUE, tags = { ConstantesTags.TAG_USUARIO })
@RequestMapping(value = USUARIOS, produces = MediaType.APPLICATION_JSON_VALUE)
public class UsuarioController {

	@Autowired
	private UsuarioService service;


	@ApiOperation(value = "Busca Usuario por id", notes = "Endpoint para CONSULTAR Usuario por ID.", response = Usuario.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_USUARIO')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<UsuarioDTO> find(@PathVariable Integer id) throws ObjectNotFoundException {

		Usuario obj = service.find(id);
		UsuarioDTO objDto = new UsuarioDTO(obj);
		return ResponseEntity.ok().body(objDto);

	}


	@ApiOperation(value = "Busca Usuario por Matricula", notes = "Endpoint para CONSULTAR Usuario por Matricula.", response = Usuario.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_USUARIO')")
	@GetMapping(value = "/matricula")
	public ResponseEntity<UsuarioDTO> findMatricula(@RequestParam(value = "value") String matricula) {

		Usuario obj = service.findByMatricula(matricula);
		UsuarioDTO objDto = new UsuarioDTO(obj);
		return ResponseEntity.ok().body(objDto);

	}

	// Método para chamar Servico de inserir objeto
	@ApiOperation(value = "Insere Usuario", notes = "Endpoint para INSERIR Usuario.", response = URI.class)
	@PreAuthorize("hasAnyAuthority('ROLE_CADASTRA_USUARIO')")
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody Usuario usuario) {

		Usuario usuarioSalvo = service.insert(usuario);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(usuarioSalvo.getId()).toUri();
		return ResponseEntity.created(uri).build();

	}

	@ApiOperation(value = "Atualiza Usuario", notes = "Endpoint para ATUALIZAR Usuario.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_USUARIO')")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Usuario> update(@Valid @RequestBody Usuario obj, @PathVariable Integer id) {
		
		obj = service.update(id, obj);
		return ResponseEntity.ok().build();

	}

	@ApiOperation(value = "Atualiza Status Usuario", notes = "Endpoint para ATUALIZAR Status do Usuario.", response = ResponseEntity.class)
	@PreAuthorize("hasAnyAuthority('ROLE_EDITA_USUARIO')")
	@PutMapping(value = "/{id}/ativo")
	public ResponseEntity<Void> atualizarPropriedadeAtivo(@Valid @RequestBody StatusUsuario obj, @PathVariable Integer id) {
		
		service.atualizarPropriedadeAtivo(id, obj);
		return ResponseEntity.noContent().build();
		
	}

	@ApiOperation(value = "Remove Usuario", notes = "Endpoint para REMOVER Usuario por Id.", response = ResponseEntity.class)
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Não é possível excluir um Usuario que tem Servidor criado"),
			@ApiResponse(code = 404, message = "Código inexistente") })
	@PreAuthorize("hasAnyAuthority('ROLE_REMOVE_USUARIO')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {

		service.delete(id);
		return ResponseEntity.noContent().build();

	}

	@ApiOperation(value = "Retorna Proprietarios até 10 linhas por pagina", notes = "Endpoint para CONSULTAR Usuario paginado.", response = UsuarioDTO.class)
	@PreAuthorize("hasAnyAuthority('ROLE_PESQUISA_USUARIO')")
	@GetMapping(value = "/page")
	public ResponseEntity<Page<UsuarioDTO>> findPage(
			@RequestParam(value = "matricula", defaultValue = "") String matricula,
			@RequestParam(value = "nomeUsuario", defaultValue = "") String nomeUsuario,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "matricula") String orderBy) {

		Page<Usuario> list = service.findPage(matricula, nomeUsuario, page, linesPerPage, direction, orderBy);
		Page<UsuarioDTO> listDto = list.map(obj -> new UsuarioDTO(obj));
		return ResponseEntity.ok().body(listDto);

	}

}
